// @flow

export const theme = {
  "palette": {
    "primary": {
      "500": "brown",
      "600": "red"
    },
    "warn": {
      "500": "gold"
    },
    "success": {
      "500": "green"
    },
    "error": {
      "500": "tomato"
    },
    "blue": {
      "500": "blue"
    },
    "green": {
      "500": "green"
    },
    "pink": {
      "500": "pink"
    },
    "red": {
      "200": "#ff4500",
      "500": "#ff0000",
      "600": "#d40000"
    },
    "yellow": {
      "500": "#ffd42a",
      "600": "#ffcc00"
    },
    "white": {
      "500": "#ffffff",
      "400": "rgba(255, 255, 255, 0.76)",
      "300": "rgba(255, 255, 255, 0.56)",
      "200": "rgba(255, 255, 255, 0.36)",
      "100": "rgba(255, 255, 255, 0.16)"
    },
    "black": {
      "500": "#000000",
      "400": "rgba(0, 0, 0, 0.76)",
      "300": "rgba(0, 0, 0, 0.56)",
      "200": "rgba(0, 0, 0, 0.36)",
      "100": "rgba(0, 0, 0, 0.16)"
    },
    "gray": {
      "500": "#f2f2f2"
    }
  },
  "typography": {
    "small": 0.9
  },
  "elevations": [
    "0 1px 3px rgba(0,0,0,0.06), 0 1px 2px rgba(0,0,0,0.12)",
    "0 3px 6px rgba(0,0,0,0.08), 0 3px 6px rgba(0,0,0,0.16)",
    "0 10px 20px rgba(0,0,0,0.08), 0 6px 6px rgba(0,0,0,0.16)",
    "0 14px 28px rgba(0,0,0,0.08), 0 10px 10px rgba(0,0,0,0.16)",
    "0 19px 38px rgba(0,0,0,0.08), 0 15px 12px rgba(0,0,0,0.1)",
    "0 38px 64px rgba(0,0,0,0.08), 0 30px 24px rgba(0,0,0,0.1)"
  ],
  "dimBase": 0.5,
  "dimAurea": 1,
  "dimDivina": 1.5,
  "unit": "rem",
  "breakpoints": {
    "tablet": [30.1, 60],
    "desktop": [60.1,120],
    "wide": [120]
  }
}

export type Theme = typeof theme;
export type WithTheme<P : {} = {}> = P & {| theme: Theme |};
