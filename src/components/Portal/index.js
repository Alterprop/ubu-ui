import { createPortal } from 'react-dom';

export function Portal(props) {
  const { selector = '.portal', children } = props;
  if (!selector || !document.querySelector(selector)) {
    const portalElem = document.createElement('div');
    portalElem.className = selector.slice(1);
    document.body.append(portalElem);
  }
  return createPortal(children, document.querySelector(selector));
}
