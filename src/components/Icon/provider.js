// @flow
import React from 'react';
export const IconContext = React.createContext<any>({});

type Props = {
  children: any,
  shapes: any,
}

export const ShapesProvider = (props: Props) => {
    return (
      <IconContext.Provider value={{ shapes: props.shapes}}>
        {props.children}
      </IconContext.Provider>
    )
}
