import React from 'react';
import ReactDOM from 'react-dom';
import renderer from 'react-test-renderer';
import { Icon } from './';
import { ShapesProvider } from './provider';
import { ThemeProvider } from 'styled-components';
import { theme } from '../../theme';
import shapes from './icons';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<ThemeProvider theme={theme}>
    <ShapesProvider shapes={shapes}>
      <Icon />
    </ShapesProvider>
  </ThemeProvider>, div);
  ReactDOM.unmountComponentAtNode(div);
});
