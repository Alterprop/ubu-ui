import React from 'react';
import { Icon } from './';
import { ShapesProvider } from './provider';
import { ThemeProvider } from 'styled-components';
import { theme } from '../../theme';
import shapes from './icons';

export default { title: 'Icon' };

export const regularIcon = () => <ThemeProvider theme={theme}>
  <ShapesProvider shapes={shapes}>
    <Icon
      size={3}
      color="blue"
      shape="facebook"
    />
  </ShapesProvider>
</ThemeProvider>
