import styled from 'styled-components';
import {
  getWidth,
  getHeight,
  getMargin,
  getColor,
} from '../_styles.utils';

export const StyledIcon = styled.svg`
  width: ${getWidth};
  height: ${getHeight};
  margin: ${getMargin};
`;

export const StyledPath = styled.path`
  fill: ${({
    theme,
    color,
    iconHue = 500,
  }) => getColor({ theme, color, textHue: iconHue })};
`;
