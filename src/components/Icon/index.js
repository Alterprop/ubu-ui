// @flow

import React from 'react';
import { IconContext } from './provider';
import {
  StyledIcon,
  StyledPath,
} from './styles';
import type { UbuiProps } from '../../types';

type IconProps = {
  color: string,
  fill: string,
  height: number,
  shape: string,
  size: number,
  stroke: string,
  strokeWidth: number,
  style: any,
  type: string,
  width: number,
};

type Props = $Shape<IconProps & UbuiProps>;

export function Icon({
  shape,
  color,
  iconHue,
  margin,
  size = 3,
  type,
  stroke,
  strokeWidth,
}: Props) {
  const { shapes } = React.useContext(IconContext);
  return (
    <StyledIcon
      xmlns="http://www.w3.org/2000/svg"
      size={size}
      className={ type ? `icon-${type}` : '' }
      viewBox="0 0 24 24"
      margin={margin}>
      <StyledPath
        color={color}
        iconHue={iconHue}
        stroke={ stroke }
        strokeWidth={ strokeWidth }
        d={shapes[shape]}
      />
    </StyledIcon>
  );
}

Icon.defaultProps = {
  color: 'currentColor',
}
