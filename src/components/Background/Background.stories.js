import React from 'react';
import {Background} from './';
import {ThemeProvider} from 'styled-components';
import {theme} from '../../theme';

import { createGlobalStyle } from 'styled-components'

const Global = createGlobalStyle`
  html,
  body,
  #root {
    margin: 0;
    padding: 0;
    height: 100%;
    width: 100%;
  }
`;

export default { title: 'Background' };

export const fullBackground = () => <ThemeProvider theme={theme}>
  <Global />
  <Background
    imageUrl="https://upload.wikimedia.org/wikipedia/commons/9/96/Jacques-Louis_David_004.jpg"
    backgroundFilter="grayscale(0.9)"
  />
  <Background
    background="rgba(32,321,124,0.46)"
    position={3}
    backgroundFilter="blur(4px)"
  />
</ThemeProvider>;
