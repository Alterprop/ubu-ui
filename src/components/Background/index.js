// @flow

import styled from 'styled-components';
import type { UbuiProps } from '../../types';
import {
  getPosition,
  getBackgroundColor
} from '../_styles.utils';

type BackgroundProps = {
  imageUrl: string,
  opacity: string,
  backgroundFilter?: string,
}

type Props = BackgroundProps & UbuiProps;

export const Background = styled.span`

  z-index: ${({ index }) => index};
  opacity: ${({ transparency }) => transparency};
  background: ${getBackgroundColor};
  ${({ imageUrl, backgroundAttachment }: Props) => imageUrl ?
      `background-image: ${`url(${imageUrl})`};
      background-size: cover;
      background-repeat: no-repeat;
      background-position: center center;
      ${ backgroundAttachment ? 'background-attachment:' + backgroundAttachment + ';' : '' };
    `
    : null };
  ${getPosition};
  filter: ${({ backgroundFilter }: Props) => backgroundFilter};
`;

Background.defaultProps = {
  index: -1,
  position: 0,
}
