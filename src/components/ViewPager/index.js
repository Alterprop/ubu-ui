
import React, { useRef, useEffect } from 'react';
import { useSprings } from 'react-spring';
import { useDrag } from 'react-use-gesture';
import { AnimatedContainer } from '../Layout';

function clamp(num, min, max) {
  return num <= min ? min : num >= max ? max : num;
}

export const ViewPager = (props) => {
  const index = useRef(0)
  const items = React.Children.toArray(props.children);
  useEffect(() => {
    index.current = props.index;
    set(i => {
      if (i < index.current - 1 || i > index.current + 1) {
        return { display: 'none' }
      }
      const x = (i - index.current) * window.innerWidth;
      return { x, scale: 1, display: 'flex' }
    })
  }, [props.index])
  const [styleProps, set] = useSprings(items.length, i => ({
    x: i * window.innerWidth,
    scale: 1,
    display: 'flex'
  }))
  const bind = useDrag(({ down, movement: [mx], direction: [xDir], distance, cancel }) => {
    if (down && distance > window.innerWidth / 2)
      cancel((index.current = clamp(index.current + (xDir > 0 ? -1 : 1), 0, items.length - 1)))
    set(i => {
      if (i < index.current - 1 || i > index.current + 1) return { display: 'none' }
      const x = (i - index.current) * window.innerWidth + (down ? mx : 0)
      const scale = down ? 1 - distance / window.innerWidth / 2 : 1
      if (typeof props.onChange === 'function') {
        props.onChange(index)
      }
      return { x, scale, display: 'flex' }
    })
  })
  return styleProps.map(({ x, display, scale }, i) => (
  <AnimatedContainer
    {...bind()}
    key={i}
    style={{ display, x, scale }}
    position={0}
    align={props.align || 'flex-end'}>
      {items[i]}
    </AnimatedContainer>
  ))
}
