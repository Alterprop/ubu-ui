import React, { useState } from 'react';
import { action } from '@storybook/addon-actions';
import {ViewPager} from './';
import {Button} from '../Button';
import {ThemeProvider} from 'styled-components';
import {theme} from '../../theme';
import {Background} from '../Background';
import Global from '../Global';

export default { title: 'ViewPager' };

const diapos = [
  {
    imageUrl: 'https://images.pexels.com/photos/296878/pexels-photo-296878.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260' ,
    backgroundFilter: 'grayscale(0.9)',
    position: 3,
    styles: {
      color: 'red'
    },
  },
  { imageUrl: 'https://images.pexels.com/photos/1509428/pexels-photo-1509428.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260' },
]

export const simpleCarousel = () => {
  const [current, setCurrent] = useState(0);
  return <ThemeProvider theme={theme}>
    <Global background="blue" />
    <ViewPager
      items={diapos}
      index={current}
      onChange={(index) => {
        setCurrent(index.current);
        action('change')
      }}
    >
      {diapos.map((diapo, index) => <Background key={index} {...diapo} /> )}
    </ViewPager>
    <div><Button onClick={() => {
      setCurrent(current === 0 ? 1 : 0);
      action(current);
    }}>Change</Button></div>
    <div>{current}</div>
  </ThemeProvider>

}

export const simpleText = () => {
  const [current, setCurrent] = useState(0);
  return <ThemeProvider theme={theme}>
    <Global background="blue" />
    <div style={{
      width: '300px',
      color: 'white',
      height: '100px',
      position: 'relative',
      overflow: 'hidden'
    }}>
      <ViewPager
        items={diapos}
        index={current}
        onChange={(index) => {
          setCurrent(index.current);
          action('change')
        }}
      >
        {['hola', 'chau'].map((text , index) => <span
          key={index}
          style={{ width: '100%', background: index === 0 ? 'blue' : 'red' }}>{text}</span>
        )}
      </ViewPager>
    </div>
    <div><Button onClick={() => {
      setCurrent(current === 0 ? 1 : 0);
      action(current);
    }}>Change</Button></div>
    <div>{current}</div>
  </ThemeProvider>

}
