import React from 'react';
import {Toolbar} from './';
import { ThemeProvider } from 'styled-components';
import { ShapesProvider } from '../Icon/provider';
import { Title } from '../Title';
import Global from '../Global';
import { Page } from '../Page';
import {theme} from '../../theme';
import shapes from '../Icon/icons';

export default { title: 'Toolbar' };

export const elementalToolbar = () => <ThemeProvider theme={theme}>
  <ShapesProvider shapes={shapes}>
    <Global size={['100%', '3000px']}/>
    <Page>
      <Toolbar
        minSize={['auto',8]}
        image="https://upload.wikimedia.org/wikipedia/commons/9/96/Jacques-Louis_David_004.jpg"
        imageRadius={4}
        margin={1}
        animated
        backButton="previous"
        contentSize={['100%', 12]}
        contentPadding={[2,4]}
        raise={3}
        zix={100}
        navbarItems={
          [
            {
              "label": "About",
              "ink": true
            },
            {
              "label": "Pricing",
              ink: true
            },
            {
              icon: "email",
              ink: true,
              label: "email",
              iconColor: 'blue'
            }
          ]
        }
      >
        <Title {...{
          pretitle: "Un acápite",
          title: "Unbiunio",
          subtitle: "Un subtítulo"
        }} />
      </Toolbar>
      <Toolbar
        title={{
          pretitle: "Un acápite",
          title: "Unbiunio",
          subtitle: "Un subtítulo"
        }}
        size={['auto',8]}
        raise={5}
        image="https://upload.wikimedia.org/wikipedia/commons/9/96/Jacques-Louis_David_004.jpg"
        imageRadius={4}
        margin={1}
        animated
        backButton="previous"
        background="yellow"
        navbarItems={
          [
            {
              "label": "About",
              "ink": true
            },
            {
              "label": "Pricing",
              ink: true
            },
            {
              icon: "email",
              ink: true,
              label: "email",
              iconColor: 'primary'
            }
          ]
        }
      >
      </Toolbar>
    </Page>
  </ShapesProvider>
</ThemeProvider>
