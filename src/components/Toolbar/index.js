// @flow
import React from 'react';
import { Figure } from '../Figure';
import { IconButton } from '../IconButton';
import { Navbar } from '../Navbar';
import { Title } from '../Title';
import type { TitleProps } from '../Title';
import { Background } from '../Background';
import { AnimatedContainer, FlexBox } from '../Layout';
import {
  headerPropsDef,
} from './_util';
import {
  _pick,
} from '../_utils';

type Props = {
  backButton: string,
  backgroundImage: string,
  backgroundFilter: string,
  buttonSize: number | string | number[] | string[],
  role?: boolean,
  title: TitleProps,
  children?: any,
  content: any,
  contentAlign?: string,
  contentPadding?: number | string | any[],
  contentMargin?: number | string | any[],
  contentSize?: number | string | number[] | string[],
  color?: string,
  iconSize?: number | string | number[] | string[],
  iconRadius?: number | string | number[] | string[],
  minSize?: number | string | number[] | string[],
  raise: number | number[],
  role?: boolean,
  size?: number,
  textColor?: string,
  textHue?: number,
  image?: string,
  imageRadius?: number | number[],
  background?: string,
  hue?: string,
  navbarItems?: any[],
  position?: number | number[] | string,
  sticky?: boolean,
  style?: any,
};

export const Toolbar = (props: Props) => {
  return (
    <AnimatedContainer
      as="header"
      {..._pick(props, headerPropsDef)}
      style={props.style}
      role={props.role ? 'banner' : null}
      position={props.position || 'relative'}
      zix={props.zix}
    >
      <FlexBox
        as="span"
        size={props.size || props.minSize}
        justify="space-between"
        align="center"
      >
        { props.backButton
          ? <IconButton
              icon={props.backButton}
              size={props.buttonSize}
              iconSize={props.iconSize}
              ink
            />
          : null
        }
        {
          props.image
            ? <Figure
                imageUrl={props.image}
                radius={props.imageRadius}
                margin={[0,2,0,1]}/>
            : null
        }
        { props.title
          ? <Title {...props.title} flex={1} />
          : null
        }
        { props.content }
        { !props.title && !props.content
          ? <span style={{ flex: 1 }}></span>
          : null
        }
        { props.navbarItems && props.navbarItems.length
            ? <Navbar
                links={props.navbarItems}
                size={['auto', Array.isArray(props.minSize) ? props.minSize[1] : props.minSize]}
                role
              />
            : null
        }
      </FlexBox>
      { props.children
        ? <FlexBox
            as="span"
            size={props.contentSize}
            align={props.contentAlign}
            padding={props.contentPadding}>
              {props.children}
          </FlexBox>
        : null
      }
      { props.backgroundImage
          ? <Background
              imageUrl={props.backgroundImage}
              backgroundFilter={props.backgroundFilter} />
          : null
      }
    </AnimatedContainer>
    )
}

Toolbar.defaultProps = {
  navItems: [],
  box: 'flex',
  buttonSize: 4,
  column: true,
  iconSize: 2,
  contentAlign: 'flex-end',
  contentPadding: 0,
  minSize: ['auto', 8],
  justify: 'center',
  selector: '#root',
}
