import React from 'react';
import {
  Heading,
  Paragraph,
  Subtitle,
  Strong,
  Text,
} from './';
import { ThemeProvider } from 'styled-components';
import { ShapesProvider } from '../Icon/provider';
import {theme} from '../../theme';
import shapes from '../Icon/icons';

export default { title: 'Typography' };

export const elementalToolbar = () => <ThemeProvider theme={theme}>
  <ShapesProvider shapes={shapes}>
    <Heading as="h1" family={['Ubuntu Condensed', 'sans-serif']}
      column
      margin={[1,0]}
      align="center"
    >
      <Text as="span">
        Uno
      </Text>
      <Subtitle weight={100} font={0.9} textCase="uppercase">Subtítulo</Subtitle>
    </Heading>
    <Heading as="h2" family={['Ubuntu Condensed', 'sans-serif']}
      column
      margin={[1,0]}
    >
      <Text as="span">
        Uno
      </Text>
      <Subtitle weight={100} font={0.9} textCase="uppercase">Subtítulo</Subtitle>
    </Heading>
    <Heading family={['Ubuntu Condensed', 'sans-serif']}
      column
      margin={[1,0]}
    >
      <Text as="span">
        Uno
      </Text>
      <Subtitle weight={100} font={0.9} textCase="uppercase">Subtítulo</Subtitle>
    </Heading>
    <Paragraph
      family={['Ubuntu Condensed', 'sans-serif']}
      weight={100}
      align="center">
      Un parágrafo con <Strong>Strong</Strong>
    </Paragraph>
  </ShapesProvider>
</ThemeProvider>
