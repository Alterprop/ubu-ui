import React from 'react';
import styled from 'styled-components';
import * as _style from '../_styles.utils';

export const Heading = styled.h3`
  align-items: ${({ align }) => align};
  color: ${_style.getColor};
  display: ${_style.getDisplay};
  flex-direction: ${_style.getDirection};
  font-family: ${_style.getFontFamily};
  font-size: ${_style.getFontSize};
  font-weight: ${({ weight }) => weight};
  justify-content: ${({ justify }) => justify};
  margin: ${_style.getMargin};
  padding: ${_style.getPadding};
  text-align: ${({ align }) => align};
  text-transform: ${({ textCase }) => textCase};
`;

Heading.defaultProps = {
  justify: 'center',
  box: 'flex',
  column: true,
  margin: 0,
  padding: 0,
}

export const H3 = styled.h3`
  display: ${_style.getDisplay};
  justify-content: ${({ justify = 'center' }) => justify};
  align-items: ${({ align }) => align};
  flex-direction: column;
  flex: ${({ flex }) => flex };
  font-family: ${_style.getFontFamily};
  font-size: ${_style.getFontSize};
  font-weight: ${({ weight }) => weight};
  margin: 0;
  color: ${_style.getColor};
  small {
    font-size: 0.8rem;
    font-weight: 400;
    text-transform: uppercase;
    flex: 1;
  }
  span {
    flex: 1;
    line-height: ${_style.getFontSize};
  }
`;

H3.defaultProps = {
  box: 'flex',
}

export const Text = styled.span`
  display: ${_style.getDisplay};
  margin: ${_style.getMargin};
  padding: ${_style.getPadding};
  flex: ${({ flex }) => flex};
  font-family: ${_style.getFontFamily};
  font-size: ${_style.getFontSize};
  font-weight: ${({ weight }) => weight};
  text-transform: ${({ textCase }) => textCase};
  text-align: ${({ align }) => align};
  letter-spacing: ${({ spacing }) => spacing};
  width: ${_style.getWidth};
`;

Text.defaultProps = {
  box: 'inline-block',
  margin: 0,
}

export const Paragraph = (props) => <Text
  as="p"
  box="block"
  {...props} />

export const Strong = (props) => <Text
  as="strong"
  box="inline"
  {...props} />

export const Small = (props) => <Text
  as="small"
  box="inline"
  {...props} />

export const Subtitle = styled(Small)`
  ${({ order }) => order ? 'order:' + order : null};
`

Strong.defaultProps = {
  weight: 600
};
