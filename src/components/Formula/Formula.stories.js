import React from 'react';
import {
  Formula,
} from './';
import {ThemeProvider} from 'styled-components';
import { ShapesProvider } from '../Icon/provider';
import Global from '../Global';
import {theme} from '../../theme';
import shapes from '../Icon/icons';

export default { title: 'Formula' };

export const simpleInput = () => <ThemeProvider theme={theme}>
  <ShapesProvider shapes={shapes}>
    <Global padding={2}/>
    <Formula
      text="h2o"
      weight={900}
      font={3}
      textCase="uppercase" />
  </ShapesProvider>
</ThemeProvider>;
