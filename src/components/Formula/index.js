// @flow

import React from 'react';
import { Text } from '../Typography';
import type { UbuiProps } from '../../types';
import { _omit } from '../_utils';

type Props = {
  ...UbuiProps,
  text: string,
  reverse?: boolean,
}

export const Formula = (props: Props) => (
  <Text {..._omit(props, ['text', 'reverse'])}>
    {props.text
      .split('')
      .map(
        (char: string, index: number) => char !== ' ' && isNaN(char)
          ? char
          : <Text key={index} as={props.reverse ? 'sup' : 'sub'}>{char}</Text>
      )
    }
  </Text>
)
