import React from 'react';
import {Avatar} from './';
import {ThemeProvider} from 'styled-components';
import {theme} from '../../theme';

export default { title: 'Avatar' };

export const simpleFigure = () => <ThemeProvider theme={theme}>
  <Avatar
    url="https://upload.wikimedia.org/wikipedia/commons/9/96/Jacques-Louis_David_004.jpg"
    dim={[12, 12]}
  />
</ThemeProvider>;
