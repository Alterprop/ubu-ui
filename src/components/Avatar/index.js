// @flow

import React from 'react';
import { Image } from 'u1b2u1';
import type { UbuiProps } from '../../types';

type AvatarProps = {
  mDim?: number | number[],
  url: string,
}

type Props = $Shape<AvatarProps & UbuiProps>;

export const Avatar = (props: Props) => <Image as="figure" {...props} />;
