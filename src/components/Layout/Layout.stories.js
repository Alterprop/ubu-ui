import React from 'react';
import {
  AnimatedContainer,
  Container,
  FlexBox,
  Flex,
} from './';
import {ThemeProvider} from 'styled-components';
import { ShapesProvider } from '../Icon/provider';
import Global from '../Global';
import {theme} from '../../theme';
import shapes from '../Icon/icons';

export default { title: 'Layout' };

export const simpleInput = () => <ThemeProvider theme={theme}>
  <ShapesProvider shapes={shapes}>
    <Global padding={2}/>
    <Container
      background="blue"
      size={['auto', 16]}>
      container
    </Container>
    <AnimatedContainer
      background="blue"
      size={['auto', 16]}>
      container
    </AnimatedContainer>
    <FlexBox
      background="red"
      align="center"
      justify="center"
      size={['auto', 16]}>
      <Flex
        box="flex"
        align="center"
        as="main"
        flex="1"
        size={['auto', '100%']}
        background="rgba(0,0,0,0.16)">flex</Flex>
      <Flex flex="2" as="aside">flex2</Flex>
    </FlexBox>
  </ShapesProvider>
</ThemeProvider>;
