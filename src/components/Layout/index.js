import styled from 'styled-components';
import * as _style from '../_styles.utils';
import { animated } from 'react-spring';

export const Container = styled.div`
  background: ${_style.getBackgroundColor};
  border: ${_style.getBorder};
  border-radius: ${_style.getRadius};
  box-shadow: ${_style.getElevation};
  color: ${_style.getColor};
  display: ${_style.getDisplay};
  font-family: ${_style.getFontFamily};
  font-size: ${_style.getFontSize};
  font-weight: ${({ weight }) => weight};
  height: ${_style.getHeight};
  margin: ${_style.getMargin};
  min-height: ${_style.getMinHeight};
  min-width: ${_style.getMinWidth};
  overflow: ${({ oflow }) => oflow};
  padding: ${_style.getPadding};
  text-align: ${({ align, box }) => align && box !== 'flex' ? align : null};
  width: ${_style.getWidth};
  z-index: ${({ zix }) => zix};
  ${_style.getPosition};
`;

export const AnimatedContainer = styled(animated.div)`
  align-items: ${({ align, box }) => align && box === 'flex' ? align : null};
  background: ${_style.getBackgroundColor};
  border: ${_style.getBorder};
  border-radius: ${_style.getRadius};
  box-shadow: ${_style.getElevation};
  color: ${_style.getColor};
  display: ${_style.getDisplay};
  flex-direction: ${_style.getDirection};
  font-family: ${_style.getFontFamily};
  font-size: ${_style.getFontSize};
  font-weight: ${({ weight }) => weight};
  height: ${_style.getHeight};
  justify-content: ${({ justify }) => justify};
  margin: ${_style.getMargin};
  min-height: ${_style.getMinHeight};
  min-width: ${_style.getMinWidth};
  overflow: ${({ oflow }) => oflow};
  padding: ${_style.getPadding};
  text-align: ${({ align, box }) => align && box !== 'flex' ? align : null};
  user-select: none;
  width: ${_style.getWidth};
  z-index: ${({ zix }) => zix};
  ${_style.getPosition};
`;

export const FlexBox = styled(Container)`
  flex-direction: ${_style.getDirection};
  align-items: ${({ align }) => align};
  justify-content: ${({ justify }) => justify};
  ${({ theme, column, responsive }) => responsive && !column ? `
      @media (max-width: ${theme.dimAurea*theme.breakpoints.mobile + theme.unit} ) {
        flex-direction: column;
        width: 100%;
        height: auto;
      }
    ` : null
  }
`

FlexBox.defaultProps = {
  box: 'flex',
  position: 'relative',
  responsive: true,
}

export const Flex = styled(FlexBox)`
  flex: ${({ flex }) => flex };
`
