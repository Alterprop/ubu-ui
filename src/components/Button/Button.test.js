import React from 'react';
import ReactDOM from 'react-dom';
import renderer from 'react-test-renderer';
import { Button } from './';
import { ThemeProvider } from 'styled-components';
import { theme } from '../../theme';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<ThemeProvider theme={theme}>
    <Button />
  </ThemeProvider>, div);
  ReactDOM.unmountComponentAtNode(div);
});

test('Button change class when hovered', () => {
  const component = renderer.create(
    <ThemeProvider theme={theme}>
      <Button
        background="primary"
        color="white"
        padding={[1,2]}
        margin={1}
        raise={1}
        radius={[1,2,1,3]}>
        <span>Button</span>
      </Button>
    </ThemeProvider>,
  );

  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
