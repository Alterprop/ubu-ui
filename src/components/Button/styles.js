// @flow

import styled from 'styled-components';
import * as _style from '../_styles.utils';
import type { UbuiPropsType } from '../../types';

export const StyledButton = styled.button`
  -webkit-appearance: none;
  cursor: pointer;
  outline: 0;
  overflow: hidden;
  position: relative;
  transition: 0.4s;

  display: ${({
    display = 'block',
    inline = true
  }) => _style.getDisplay({ display, inline })};

  align-items: ${({ align, }: UbuiPropsType) => align};

  & + button,
  & + a {
    margin-left: ${({ full, theme }) => !full ? '1' + theme.unit : null};
  }

  text-align: ${({
    as
  }) => as === 'a' ? 'center' : null };

  vertical-align: ${({
    as,
    icon,
  }) => as === 'a' || icon ? 'middle' : null };

  width: ${props => props.full ? '100%' : _style.getWidth(props) };
  height: ${_style.getHeight};
  background: ${props => _style.getBackgroundColor(props) || 'transparent'};
  color: ${props => _style.getColor(props) || 'inherit'};
  border: ${props => _style.getBorder(props) || 0};
  border-radius: ${_style.getRadius};
  padding: ${_style.getPadding};
  margin: ${_style.getMargin};
  font-size: ${_style.getFontSize};
  box-shadow: ${_style.getElevation};
  min-width: ${_style.getMinWidth};

  &:disabled {
    background: ${({
      theme,
    }) => theme
      ? theme.palette.gray[500]
      : null
    };
    color: ${({
      theme,
    }) => theme
      ? theme.palette.black[300]
      : null
    };
  }

  span,
  strong {
    flex: 1;
  }

  svg {
    transition: 0.2s;
    display: inline-block;
    vertical-align: middle;

    top: ${({
      icon,
      count,
    }) => icon && count && count > 1 ? '50%' : null };

    &:nth-of-type(1) {
      margin-right: ${({
        theme,
        icon,
      }) => theme && !icon
        ? theme.dimBase + theme.unit
        : null
      };
      left: ${({
        icon,
        count,
      }) => icon && count && count > 1 ? '50%' : null };
      transform: ${({
        icon,
        count,
      }) => icon && count && count > 1 ? 'translate(-50%, -50%)' : null };
    }

    position: ${({
      icon,
      count,
    }) => icon && count && count > 1 ? 'absolute' : null };

    &:nth-of-type(2) {
      transform: ${({
        icon,
        count,
      }) => icon && count && count > 1 ? 'translate3d(100%, -50%, 0) scale(1.3)' : 'translate3d(100%, 0, 0)' };
      opacity: 0;
      margin-left: ${({
        theme,
        icon,
      }) => theme && !icon
        ? theme.dimBase + theme.unit
        : null
      };
      left: ${({
        icon,
        count,
      }) => icon && count && count > 1 ? '50%' : null };
    }
  }

  &:hover {
    &:not(:disabled) {
      box-shadow: ${_style.getElevationHover};

      background: ${({
        theme,
        background,
        hue = 500
      }) => background && theme && theme.palette[background] && theme.palette[background][hue + 100]
        ? theme.palette[background][hue + 100]
        : null
      };
      svg {

        &:nth-of-type(1) {
            transform: ${({ icon, count }) => icon && count && count > 1
              ? 'translate3d(-100%, -50%, 0)'
              : null
            };
            opacity: ${({ icon, count }) => icon && count && count > 1
              ? 0
              : null
            };
            transition: 0.2s;
        }

        &:nth-of-type(2) {
          transform: ${({
            icon,
            count,
          }) => icon && count && count > 1 ?  'translate(-50%, -50%)' : 'translate3d(0, 0, 0)' };
          opacity: 1;
          transition: 0.2s;
        }
      }
    }
  }
`;

StyledButton.defaultProps = {
  padding: [1,3],
}
