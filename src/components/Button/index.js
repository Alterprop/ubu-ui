// @flow

import React from 'react';
import Ink from 'react-ink';
import {
  StyledButton
} from './styles';

import type { UbuiProps } from '../../types';

type ButtonProps = {
  onClick?: Function,
}

type Props = ButtonProps & UbuiProps;

export const Button = (props: Props) => {
  const count = React.Children.toArray(props.children).filter(child => child).length;
  return <StyledButton
      {...props}
      count={count}>
    { props.children }
    { props.ink ? <Ink /> : null }
  </StyledButton>
};
