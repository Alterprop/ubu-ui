import React from 'react';
import { action } from '@storybook/addon-actions';
import { Button } from './';
import { ThemeProvider } from 'styled-components';
import { theme } from '../../theme';

export default { title: 'Button' };

export const regularButton = () => <ThemeProvider theme={theme}>
  <div>
    <Button
      radius={24}
      ink
      onClick={action('click')}>
      <span>Button</span>
    </Button>
  </div>
</ThemeProvider>;

export const primaryButton = () => <ThemeProvider theme={theme}>
  <div>
    <Button
      background="primary"
      color="white"
      padding={[1,2]}
      margin={1}
      raise={1}
      radius={[1,2,1,3]}
      onClick={action('click')}>
      <span>Button</span>
    </Button>
  </div>
</ThemeProvider>;

export const fullButton = () => <ThemeProvider theme={theme}>
  <div>
    <Button
      background="primary"
      color="white"
      padding={[1,2]}
      raise={1}
      full
      onClick={action('click')}>
      <span>Button</span>
    </Button>
  </div>
</ThemeProvider>;

export const grossButton = () => <ThemeProvider theme={theme}>
  <div>
    <Button
      background="primary"
      color="white"
      padding={[3,6]}
      raise={1}
      radius={4}
      onClick={action('click')}>
      <span>Button</span>
    </Button>
  </div>
</ThemeProvider>;
