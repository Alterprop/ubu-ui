import React from 'react';
import {Figure} from './';
import {ThemeProvider} from 'styled-components';
import {theme} from '../../theme';

export default { title: 'Figure' };

export const simpleFigure = () => <ThemeProvider theme={theme}>
  <Figure
    imageUrl="https://upload.wikimedia.org/wikipedia/commons/9/96/Jacques-Louis_David_004.jpg"
    size={16}
    rounded
  />
</ThemeProvider>;
