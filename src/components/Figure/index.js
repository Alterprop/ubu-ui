// @flow

import styled from 'styled-components';
import * as _style from '../_styles.utils';
import type { UbuiProps } from '../../types';

type FigureProps = {
  avatar?: boolean,
  imageUrl: string,
}

type Props = FigureProps & UbuiProps;

export const Figure = styled.figure`
  margin: ${(props: Props) => _style.getMargin(props) || 0};
  background: ${_style.getBackgroundColor};
  width: ${_style.getWidth};
  height: ${_style.getHeight};
  border: ${_style.getBorder};
  border-radius: ${_style.getRadius};
  flex: ${({ avatar, flex }: Props) => !avatar && flex };
  min-height: ${_style.getMinHeight};
  min-width: ${_style.getMinWidth};

  background-image: ${({ imageUrl }: Props) => imageUrl ? 'url(' + imageUrl + ')' : null};

  ${({ imageUrl }: Props) => imageUrl
    ? `background-size: cover;
        background-position: center center;
        background-repeat: no-repeat;`
    : null
  };

  img {
    max-width: 100%;
  }
`

Figure.defaultProps = {
  size: [6,6]
}
