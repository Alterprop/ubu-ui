export const monthPropsDef = {
  background: 'monthBackground',
  locale: 'locale',
};

export const dayListPropsDef = [
  'hues',
  'backgrounds',
  'colors',
  'radius',
  'rounded',
  'numberSize'
];

export const getChunks = (arr) => {
  const tempArr = [];
  const chunkSize = 7;
  for (let i=0,len=arr.length; i<len; i+=chunkSize) {
    tempArr.push(arr.slice(i,i+chunkSize));
  }
  return tempArr;
}

export const getStartDay = ({ month, year }) => {
  return (new Date(year, month, 1)).getDay();
}

export const getMonthDays = (
  year,
  month,
  locale,
  type='current',
) => {
  const options = {
    weekday: 'long',
    year: 'numeric',
    month: 'long',
    day: 'numeric'
  };
  return Array.apply(0,
    Array(
      (new Date(year, month, 0)).getDate())
    ).map((_, index) => {
      const day = index + 1;
      const _date = (new Date(year, month-1, day));
      return {
        day,
        monthName: _date.toLocaleString(locale, { month: 'long' }),
        month: _date.getMonth(),
        localeString: _date.toLocaleDateString(locale, options),
        year: _date.getFullYear(),
        type,
        today: _date.getTime() === (new Date()).getTime()
      }
    }
  )
}

export const getMonths = (
  locale,
  format='long'
) => {
  const _date = new Date();
  let months = [];
  for(let i = 0; i <= 11; i++) {
    _date.setMonth(i);
    let monthName = _date.toLocaleString(locale, { month: format });
    months = [...months, { name: `${monthName.charAt(0).toUpperCase()}${monthName.slice(1)}`, number: i}];
  }
  return months;
}

export const getDayNames = (
  date,
  locale,
  format = 'long'
) => {
  // Random day starts in sunday
  const _date = new Date('2019-12-01');
  let days = [];
  for(let i = 1; i <= 7; i++) {
    _date.setDate(i);
    let dayName = _date.toLocaleString(locale, { weekday: format });
    days = [...days, `${dayName.charAt(0).toUpperCase()}${dayName.slice(1)}`];
  }
  return days;
}

export const getDays = (
  date,
  locale=navigator.language || navigator.userLanguage
) => {
  const _date = date && date instanceof Date ? date : new Date();
  const year = _date.getFullYear();
  const month = _date.getMonth();
  const current = _date.getDate();

  const firstDay = getStartDay({ month, year });

  const previousMonth = firstDay > 0
    ? getMonthDays(year, month, locale, 'prev')
        .slice(-firstDay)
    : getMonthDays(year, month, locale, 'prev')
        .slice(-7);

  const currentMonth = getMonthDays(year, month + 1, locale, 'current', current);
  const nextMonthDays =  42-(currentMonth.length + firstDay);
  const nextMonth = firstDay < 7 && nextMonthDays > 0
    ? getMonthDays(year, month + 2, locale, 'next')
        .slice(0, nextMonthDays)
    : getMonthDays(year, month + 2, locale, 'next')
        .slice(0, 7);

  return [...previousMonth, ...currentMonth, ...nextMonth];
}

export const getData = (
  date,
  locale
) => ({
  days: getDays(date, locale),
  months: getMonths(locale),
  weekDays: getDayNames(date, locale),
  weekDaysAbbr: getDayNames(date, locale, 'short'),
})

export const isInRange = (
  day,
  [first, last],
) => {
  const current = new Date(day.year, day.month, day.day);
  if (first && !last) {
    return current.getTime() === first.getTime();
  } else if (last) {
    return current.getTime() >= first.getTime() && current.getTime() <= last.getTime()
  }
}

export const isFirst = (
  day,
  [first]
) => {
  const current = new Date(day.year, day.month, day.day);
  return current.getTime() === (first && first.getTime());
}

export const isLast = (
  day,
  value
) => {
  const current = new Date(day.year, day.month, day.day);
  return current.getTime() === (value && value[1] && value[1].getTime());
}

export const getValue = (
  day,
  value,
  range,
) => {
  const [first, last] = value;
  const current = new Date(day.year, day.month, day.day);
  if (range && !last) {
    if (!first || current.getTime() > first.getTime()) {
      return [...value, current]
    }
  }
  return [current];
}
