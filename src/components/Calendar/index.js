// @flow

import React, { useState, useEffect } from 'react';
import { IconButton } from '../IconButton';
import { Title } from '../Title';
import type { UbuiProps } from '../../types';
import {
  getData,
  isInRange,
  getValue,
  isLast,
  isFirst,
  monthPropsDef,
  dayListPropsDef,
} from './_util';
import {
  _pick,
} from '../_utils';

import {
  Row,
  CurrentMonth,
  Day,
  DayNumber,
  StyledCalendar
} from './styles';

type CalendarProps = {
  monthBackground: string | string[],
  onPrevClick: Function,
  range: boolean,
  selected: any,
  locale: string,
  onNextClick: Function,
  days: any[],
  value: any[],
  onDayClick: Function,
  gap: number,
  backgrounds: any,
  colors: any,
  hues: any,
  radius: number | number[],
  rounded: boolean,
  numberSize: number | number[],
  numberMargin: number | number[],
  numberPadding: number | number[],
  date: string,
  onChange: Function,
}

type Props = CalendarProps & UbuiProps;


const Month = (props: Props) => {
  return (
    <CurrentMonth
      background={props.monthBackground}
    >
      <IconButton
        icon="previous"
        iconSize={2.5}
        size={6}
        onClick={props.onPrevClick}
        ink />
      <Title
        title={props.selected.toLocaleString(props.locale, { month: 'long' })}
        subtitle={props.selected.toLocaleString(props.locale, { year: 'numeric' })}
        align="center"
      />
      <IconButton
        icon="next"
        iconSize={2.5}
        size={6}
        onClick={props.onNextClick}
        ink />
    </CurrentMonth>
  )
}

const DayList = (props: Props) => {
  const {
    days,
    value,
    onDayClick,
  }: Props = props;
  return (
    <Row gap={props.gap}>
      { days.map(day => {
        const inRange = isInRange(day, value);
        return (
          <Day
            key={`${day.monthName}-${day.day}`}
            margin={props.margin}
            padding={props.padding}
            gap={props.gap}
            onClick={() => onDayClick(day)}>
            <DayNumber
              background={props.backgrounds[inRange ? 0 : 1]}
              color={inRange ? props.colors.selected : props.colors[day.type]}
              textHue={inRange ? props.hues.selected : props.hues[day.type]}
              first={isFirst(day, value)}
              last={isLast(day, value)}
              isRange={value.length === 2}
              radius={props.radius}
              rounded={props.rounded ? 1 : 0}
              size={props.numberSize}
            >{day.day}</DayNumber>
          </Day>
        )
      })}
    </Row>
  )
}

export const Calendar = (props: Props) => {
  const [selected, setSelected] = useState(new Date(props.date || Date.now()));

  const {
    onChange,
    range,
  } = props;
  const [value, setValue] = useState(props.value || []);
  useEffect(() => {
    const change = () => {
      const [first, last] = value;

      if ((range && last && last.getMonth() !== selected.getMonth()) || (first && first.getMonth() !== selected.getMonth())) {
        setSelected(new Date((last || first).getFullYear(), (last || first).getMonth()))
      }

      if (typeof onChange === 'function' && value.length) {
        onChange(range ? value : [first]);
      }
    }
    change();
  }, [value])

  const calendar = getData(selected, props.locale);
  const currentMonth = calendar.months.find(
    ({ number }) => number === selected.getMonth()
  );
  return <StyledCalendar
      size={props.size}
      background={props.background}
      color={props.color}>

        <Month
          {..._pick(props, monthPropsDef)}
          selected={selected}
          onPrevClick={() => {
              const { number } = currentMonth;
              const year = number === 0 ? selected.getFullYear() - 1 : selected.getFullYear();
              const month = number === 0 ? 11 : number - 1;
              setSelected(new Date(year, month))
            }}
          onNextClick={() => {
              const { number } = currentMonth;
              const year = number === 11 ? selected.getFullYear() + 1 : selected.getFullYear();
              const month = number === 11 ? 0 : number + 1;
              setSelected(new Date(year, month))
            }}
        />

        <Row gap={props.gap}>
          {calendar.weekDaysAbbr.map((day) => <li key={day} style={{ flex: 1, textAlign: 'center', listStyle: 'none', height: '48px', lineHeight: '48px' }}>{day}</li>)}
        </Row>

         <DayList
            {..._pick(props, dayListPropsDef)}
            days={calendar.days}
            value={value}
            onDayClick={(day) => setValue(getValue(day, value, props.range))}
            margin={props.numberMargin}
            padding={props.numberPadding}
          />
  </StyledCalendar>
}

Calendar.defaultProps = {
  backgrounds: ['primary', 'transparent'],
  monthBackground: 'primary',
  columnWidth: 'auto',
  numberPadding: [1,1],
  numberMargin: 0,
  gap: 0,
  hues: {
    selected: 500,
    current: 400,
    prev: 200,
    next: 200,
  },
  colors: {
    selected: 'white',
    current: 'black',
    prev: 'black',
    next: 'black',
  },
  numberSize: 6,
  margin: 0,
  padding: 0,
}
