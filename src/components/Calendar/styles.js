import styled from 'styled-components';
import * as _style from '../_styles.utils';

export const Row = styled.ul`
  display: grid;
  grid-template-columns: ${({ columnWidth = 'auto' }) => {
    return Array.apply(0, Array(7))
      .reduce((acc) => `${acc} ${columnWidth}`.trim(), '');
  }};
  grid-auto-rows: 1fr;
  margin: 0;
  padding: ${_style.getPadding};
  gap: ${({ gap }) => gap};
`;

Row.defaultProps = {
  padding: 0,
}

export const CurrentMonth = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  background: ${_style.getBackgroundColor};
  h3 {
    text-transform: uppercase;
  }
`

export const Day = styled.li`
  list-style: none;
  flex: 1;
  text-align: center;
  padding: ${_style.getPadding};
  margin: ${_style.getMargin};
`;

export const DayNumber = styled.span`
  display: inline-block;
  width: ${_style.getWidth};
  height: ${_style.getHeight};
  line-height: ${_style.getHeight};
  background: ${_style.getBackgroundColor};
  border-radius: ${({
    radius,
    theme,
    rounded
  }) => rounded ? '50%' : _style.getRadius({ radius, theme })};
  color: ${_style.getColor};
  transition: .2s;
`;

export const StyledCalendar = styled.div`
  width: ${_style.getWidth};
  height: ${_style.getHeight};
  background: ${_style.getBackgroundColor};
  color: ${_style.getColor};
`;
