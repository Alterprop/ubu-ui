import React from 'react';
import {Calendar} from './';
import {ThemeProvider} from 'styled-components';
import { ShapesProvider } from '../Icon/provider';
import Global from '../Global';
import {theme} from '../../theme';
import shapes from '../Icon/icons';

export default { title: 'Calendar' };

export const rawCalendar = () => <ThemeProvider theme={theme}>
  <ShapesProvider shapes={shapes}>
    <Global />
    <Calendar locale="es-UY" />
  </ShapesProvider>
</ThemeProvider>;
