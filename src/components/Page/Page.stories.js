import React, { useState } from 'react';
import { Page } from './';
import {ThemeProvider} from 'styled-components';
import {theme} from '../../theme';
import Global from '../Global';
import { Button } from '../Button';

export default { title: 'Page' };

export const regularPage = () => {
  const [show, setVisibility] = useState(false);
  const _handle = () => setVisibility(!show);
  return (
    <ThemeProvider theme={theme}>
      <Global />
      <Button onClick={_handle}>Change</Button>
      { show ? <Page
        background="red"
        animation="toLeft"
      >
      Página
      </Page> : null }
      { !show ? <Page
        background="blue"
        animation="toLeft"
      >
      Página
      </Page> : null }
    </ThemeProvider>
  )
}
