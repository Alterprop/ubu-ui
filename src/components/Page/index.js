// @flow
import React, { useState } from 'react';
import { useTransition } from 'react-spring';
import animations from '../transitions.json';
import {
  AnimatedContainer,
  Flex,
} from '../Layout';

type Props = {
  animation: boolean,
  children: any,
}

export const Page = (props: Props) => {
  // eslint-disable-next-line no-unused-vars
  const [show, set] = useState(true);
  const transitions = useTransition(
    show,
    null,
    animations[props.animation]
  );
  return <Flex
    as="main"
    flex={1}
    {...(props.animation ? {} : props)}>
    { props.animation ?
        transitions.map(
        ({
          item,
          key,
          props: _styleProps
        }) => item && <AnimatedContainer
          key={key}
          style={_styleProps}
          {...props}>
        </AnimatedContainer>
      ) : props.children
    }
  </Flex>
}

Page.defaultProps = {
  size: '100%',
  oflow: 'auto',
  column: 1,
}
