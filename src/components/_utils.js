export { unflatten } from 'flat';

export const filterProps = (props) => Object.keys(props)
  .reduce((acc, prop) => ![
    'align',
    'background',
    'bordered',
    'borderColor',
    'borderWidth',
    'borderStyle',
    'color',
    'column',
    'count',
    'font',
    'flex',
    'hue',
    'iconic',
    'ink',
    'justify',
    'margin',
    'padding',
    'raise',
    'radius',
    'rounded',
    'row',
    'size',
    'textHue',
    'title',
  ].includes(prop) ? ({
      ...acc,
      [prop]: props[prop]
    })
    : acc, {});

export const _omit = (props, filter) => Object.keys(props)
  .filter(prop => !filter.includes(prop))
  .reduce((acc, prop) => ({
    ...acc,
    [prop]: props[prop]
  }), {});

export const _pickProps = (props, filter = []) => Object.keys(props)
  .filter(prop => filter.includes(prop))
  .reduce((acc, prop) => ({
    ...acc,
    [prop]: props[prop]
  }), {});

export const _pickAlias = (props, filter = {}) => Object.entries(filter)
  .reduce((acc, [key, value]) => ({
    ...acc,
    [key]: props[value]
}), {});

export function _pick(props, filter) {
  return Array.isArray(filter)
    ? _pickProps.apply(null, arguments)
    : _pickAlias.apply(null, arguments);
}

export const _getPropsFromDef = (def) => Object.keys(def)
  .map(key => def[key]);

const _getLabel = ({
  label
}) => typeof label === 'object'
  ? label[navigator.language.toLowerCase()] ? label[navigator.language.toLowerCase()] : label[label.default]
  : label

const _getItem = (input) => ({
  ...input,
  name: input.key,
  label: _getLabel(input),
});

const _getItems = ({
  items,
}) => {
  return Array.isArray(items)
    ? items.map(([key, item]) => _getItem({ ...item, key }))
    : _getItem(items)
}
export const _getByType = (input) => {
  return input.type === 'array' && input.items ? ({
  ...input,
  label: _getLabel(input),
  items: _getItems(input),
}) : _getItem(input)
}

export const toInput = (
  flatObj = {},
) => Object.keys(flatObj)
  .reduce(
    (acc, key) => ([
      ...acc,
      _getByType({
        key,
        ...flatObj[key]
      })
    ])
, [])

export const flatten = (
  schema,
  keys = [],
) => {

  const _rec = (
    obj,
    parent,
  ) => Object.entries(obj)

    .reduce((acc, [ key, val ]) => {

      const schemaDef = keys[val.type] || keys;

      const _shouldCondition = (o) => schemaDef.length
        ? !Object.keys(o).every(oi => schemaDef.includes(oi))
        : typeof o === 'object'
      const _shouldRecurse = _shouldCondition(val)

      const finalKey = parent
        ? `${parent}.${key}`
        : key

      if (val.type === 'array') {
        return [
          ...acc,
          [
            finalKey,
            {
              ...val,
              items: _shouldCondition(val.items)
                ? _rec(val.items, finalKey)
                : { ...val.items, key: finalKey }
            }
          ]
        ];
      } else if (_shouldRecurse) {
        return [
          ...acc,
          ..._rec(val, finalKey)
        ];
      }

      return [ ...acc, [ finalKey, val ]];
    }, [])

  return Object.fromEntries(_rec(schema));
}

export const schemaToInput = (
  schema,
  schemaDef
) => toInput(flatten(schema, schemaDef))
