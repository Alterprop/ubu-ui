import React from 'react';
import { action } from '@storybook/addon-actions';
import { List } from './';
import { Icon } from '../Icon';
import { Button } from '../Button';
import {ThemeProvider} from 'styled-components';
import { ShapesProvider } from '../Icon/provider';
import Global from '../Global';
import {theme} from '../../theme';
import shapes from '../Icon/icons';

export default { title: 'List' };

export const regularList = () => <ThemeProvider theme={theme}>
  <ShapesProvider shapes={shapes}>
    <Global />
    <List
      size="40"
      items={[{
        name: 'uno',
        title: 'Daniel Ferreira',
        subtitle: 'Web developer',
        image: "https://upload.wikimedia.org/wikipedia/commons/9/96/Jacques-Louis_David_004.jpg",
        rounded: true,
        ink: true,
        raise: 1,
        nextColor: 'yellow',
        background: ['red', 'blue'],
        gradient: 'to bottom right',
        iconColor: 'white',
        color: 'white',
        footer: <Button
          radius={24}
          size={4}
          padding={0}
          icon
          ink
          onClick={action('button')}>
          <Icon
            shape="facebook"
            color="white"
            iconHue={500}
            size={2}
          />
        </Button>
      },
      {
        name: 'dos',
        title: 'Pepito lito',
        subtitle: 'Web developer',
        image: "https://upload.wikimedia.org/wikipedia/commons/9/96/Jacques-Louis_David_004.jpg",
        raise: 1,
        itemMargin: [1,0],
        ink: true,
        rounded: true,
        action: action('item')
      },
      {
        name: 'uno',
        title: 'Daniel Ferreira',
        subtitle: 'Web developer',
        shape: "email",
        shapeColor: "white",
        rounded: true,
        ink: true,
        raise: 1,
        nextColor: 'yellow',
        background: ['hotpink', 'magenta'],
        gradient: 'to bottom right',
        iconColor: 'white',
        color: 'white',
      }

    ]} onItemClick={(item) => console.log(item)} />
  </ShapesProvider>
</ThemeProvider>;
