export const itemPropsDef = [
  'raise',
  'background',
  'gradient',
  'hue',
  'color',
  'textHue',
];

export const buttonPropsDef = [
  'icon',
  'iconSize',
  'nextIcon'
]
