// @flow

import styled from 'styled-components';
import * as _style from '../_styles.utils';
import { animated } from 'react-spring';
import type { UbuiProps } from '../../types';

export const StyledListItemContent = styled.span`
  display: flex;
  align-items: center;
  min-height: ${({ theme }: UbuiProps) => theme.dimAurea*4 + theme.unit};
  position: relative;
  padding: ${_style.getPadding};
  h3 {
    flex: 1;
    padding: ${({ theme, image }: UbuiProps) => image ? '0 ' + theme.dimAurea + theme.unit : null };
  }
`;

StyledListItemContent.defaultProps = {
  padding: [0, 1],
}

export const StyledListItemFooter = styled.footer`
  display: flex;
  padding: ${_style.getPadding};
  margin: ${_style.getMargin};
  border-top: 1px solid rgba(0, 0, 0, 0.056);
`;

StyledListItemFooter.defaultProps = {
  padding: [0.5],
}

export const StyledListItem = styled(animated.li)`
  display: flex;
  flex-direction: column;
  list-style: none;
  box-shadow: ${_style.getElevation};
  background: ${_style.getBackgroundColor};
  color: ${_style.getColor};
  margin: ${_style.getMargin};
`;

export const StyledList = styled.ul`
  margin: 0;
  min-width: ${({ theme }) => theme.dimAurea*5.5 + theme.unit };
  max-width: 100%;
  width: ${_style.getWidth};
`;
