// @flow

import React from 'react';
import { useSpring } from 'react-spring';
import type { UbuiProps } from '../../types';
import { Title } from '../Title';
import { Icon } from '../Icon';
import { Avatar } from '../Avatar';
import { IconButton } from '../IconButton';
import Ink from 'react-ink';
import animations from '../animations';
import {
  buttonPropsDef,
  itemPropsDef,
} from './_util';
import {
  _pick,
} from '../_utils';
import {
  StyledListItemContent,
  StyledListItemFooter,
  StyledListItem,
  StyledList,
} from './styles';

type ListProps = {
  action: Function,
  contentPadding: number | number[],
  footer: any,
  footerPadding: number | number[],
  footerMargin: number | number[],
  image: string,
  items: any,
  itemMargin: number | number[],
  itemPadding: number | number[],
  nextIcon: string,
  nextSize: number | number[],
  onClick: Function,
  onItemClick: Function,
  pretitle: string,
  shape: string,
  shapeColor: string,
  subtitle: string,
  title: string,
};

type Props = ListProps & UbuiProps;

export const ListItem = (props: Props) => {
  const _styleProps = useSpring(animations['bottom']);
  return (
    <StyledListItem
      {..._pick(props, itemPropsDef)}
      style={_styleProps}
      margin={props.itemMargin}>

      <StyledListItemContent
        raise={props.raise}
        image={props.image}
        onClick={props.onClick}
        padding={props.contentPadding}>

        { props.image
            ? <Avatar
                imageUrl={props.image}
                size={props.size || 6}
                rounded={props.rounded} />
            : null }

        { props.shape
            ? <Icon
                shape={props.shape}
                color={props.shapeColor}
                margin={[0,1]}
                size={props.size || 4} />
            : null }

        <Title
          title={props.title}
          subtitle={props.subtitle}
          pretitle={props.pretitle} />

        { props.ink ? <Ink /> : null }
        { typeof props.action === 'function'
            ? <IconButton
                {..._pick(props, buttonPropsDef)}
                onClick={props.action}
                size={props.nextSize}
                ink />
            : null
        }
      </StyledListItemContent>

      { props.footer
        ? <StyledListItemFooter
            padding={props.footerPadding}
            margin={props.footerMargin}
          >
            { props.footer }
          </StyledListItemFooter>
        : null
      }
    </StyledListItem>
  )
}

ListItem.defaultProps = {
  icon: 'next',
  iconSize: 2,
  nextSize: 4,
}

export const List = (props: Props) => (
    <StyledList size={props.size}>
      { props.items.map((item, index) => <ListItem key={item.id || index} {...item} onClick={() => props.onItemClick(item)} /> ) }
    </StyledList>
  )
