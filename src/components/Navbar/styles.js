import styled from 'styled-components';
import * as _style from '../_styles.utils';
import { Button } from '../Button';

export const NavLinkContent = styled.span`
  align-items: ${({ align }) => align};
  border-radius: ${_style.getRadius};
  display: ${_style.getDisplay};
  flex: ${({ flex }) => flex};
  margin: ${_style.getMargin};
  overflow: hidden;
  padding: ${_style.getPadding};
  position: relative;
  ${
    ({ deco }) => deco
    ? `
      &::before {
        content: '';
        height: 3px;
        width: 100%;
        position: absolute;
        background: currentColor;
        bottom: 0;
        left: -100%;
      }
    `
    : null
  };
`;

NavLinkContent.defaultProps = {
  box: 'flex',
  align: 'center',
  flex: 1,
}

export const NavLinkElement = styled(Button)`
  align-items: ${({ align }) => align};
  background: ${_style.getBackgroundColor};
  border-radius: ${_style.getRadius};
  color: ${_style.getColor};
  cursor: pointer;
  display: ${_style.getDisplay};
  height: 100%;
  justify-content: ${({ justify }) => justify};
  margin: ${_style.getMargin};
  padding: ${_style.getPadding};
  position: relative;
  text-decoration: none;
  width: ${_style.getWidth};

  &:hover {
    & > span {
      &::before {
        left: 0;
        transition: 0.3s;
      }
    }
  }
  & > span {
    &::before {
      left: ${({ active }) => active ? '0' : null};
      transition: 0.3s;
    }
  }

  span {
    height: 100%;
  }
`;

NavLinkContent.defaultProps = {
  box: 'flex',
  align: 'center',
  justify: 'center',
}
