// @flow

import React from 'react';
import Ink from 'react-ink';
import { Icon } from '../Icon';
import { Flex } from '../Layout';
import type { UbuiProps } from '../../types';
import {
  NavLinkContent,
  NavLinkElement,
} from './styles';
import {
  navPropsDef,
  itemPropsDef,
} from './_util';
import {
  _pick,
} from '../_utils';

export type ItemProps = {
  ...UbuiProps,
  icon: string,
  iconSize: number,
  iconColor: string,
  itemPadding?: number | number[],
  itemMargin?: number | number[],
  column: boolean,
  contentPadding?: number | number[],
  contentMargin?: number | number[],
  name: string,
  label: string,
  link: string,
  ink: boolean,
  href: string,
  onClick: Function,
  onItemClick: Function,
};

type NavProps = {
  links: ItemProps[],
  role: boolean,
  itemPadding?: ?number | ?number[],
  itemMargin?: ?number | ?number[],
  contentPadding?: number | number[],
  contentMargin?: number | number[],
  onItemClick?: Function,
  weight: number | string,
  direction: string,
};

type Props = $Shape<NavProps & UbuiProps>;

export const Navbar = (props: Props) => {
  return (
    <Flex
        as="nav"
        role={props.role ? 'navigation' : null}
        {..._pick(props, navPropsDef)}
      >
      {props.links.map(
        (item: ItemProps, index: number) => <NavLinkElement
          key={`${item.name}-${index}`}
          {..._pick(item, itemPropsDef)}
          active={index===0}
          title={item.name}
          padding={props.contentPadding}
          margin={props.contentMargin}
          as={item.href ? 'a' : null}
          full={props.column}
          onClick={item.onClick ? item.onClick : props.onItemClick}>
          <NavLinkContent
            padding={props.itemPadding}
            margin={props.itemMargin}
            deco={!props.column} >
            { item.icon
                ? <Icon
                  shape={item.icon }
                  size={item.iconSize || 2}
                  color={item.iconColor}
                  margin={[0,1,0,0]} />
                : null }
            {item.label}
          </NavLinkContent>
          { props.ink ? <Ink /> : null }
        </NavLinkElement>
      )}
    </Flex>
  );
};

Navbar.defaultProps = {
  links: [],
  role: false,
  color: 'inherit',
  itemMargin: [0, 1],
  contentPadding: 0,
  contentMargin: 0,
  weight: 600,
}
