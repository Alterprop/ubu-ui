export const navPropsDef = [
  'background',
  'color',
  'column',
  'fontWeight',
  'margin',
  'minSize',
  'padding',
  'radius',
  'raise',
  'size',
]

export const itemPropsDef = [
  'active',
  'background',
  'color',
  'column',
  'href',
  'icon',
  'radius',
]
