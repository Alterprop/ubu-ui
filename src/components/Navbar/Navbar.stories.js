import React from 'react';
import { action } from '@storybook/addon-actions';
import { Navbar } from './';
import { ThemeProvider } from 'styled-components';
import { ShapesProvider } from '../Icon/provider';
import Global from '../Global';
import {theme} from '../../theme';
import shapes from '../Icon/icons';

export default { title: 'Navbar' };

export const single = () => <ThemeProvider theme={theme}>
  <ShapesProvider shapes={shapes}>
    <Global />
    <Navbar links={[{
          href: 'about',
          name: 'about',
          label: 'About',
        },
        {
          href: 'pricing',
          name: 'pricing',
          label: 'Pricing',
        },
        {
          href: 'pricing',
          name: 'pricing',
          label: 'Pricing',
          icon: 'email',
          iconColor: 'red'
        }]}
        role
        background={['red', 'blue']}
        color="white"
        radius={0.5}
        margin={2}
        size={['auto', 8]}
        minSize={[null, 8]}
        raise={1}
        ink/>

      <Navbar links={[{
          href: 'about',
          name: 'about',
          label: 'About',
          background: 'yellow',
          radius: [0.5, 0, 0, 0.5]
        },
        {
          href: 'pricing',
          name: 'pricing',
          label: 'Princing',
          onClick: action('clickOnTheItem'),
          background: 'gold'
        },
        {
          href: 'pricing',
          name: 'pricing',
          label: 'Princing',
          icon: 'email',
          iconColor: 'white',
          ink: true,
          background: 'yellow',
          radius: [0, 0.5, 0.5, 0]
        }]}
          role
          size={[30, 'auto']}
          color="primary"
          margin={2}
          itemPadding={1}
          ink
          align="flex-start"
        />
        <Navbar links={[{
            href: 'about',
            name: 'about',
            label: 'About',
          },
          {
            href: 'pricing',
            name: 'pricing',
            label: 'Princing',
            onClick: action('clickOnTheItem')
          },
          {
            href: 'pricing',
            name: 'pricing',
            label: 'Princing',
            icon: 'email',
            iconColor: 'yellow',
            ink: true,
            radius: 0.5
          }]}
            role
            size={[30, 'auto']}
            color="primary"
            margin={2}
            itemPadding={[1,0]}
            itemMargin={0}
            background="#f9f9f9"
            contentPadding={[1,2]}
            column
            ink
            align="flex-start"
          />
  </ShapesProvider>
</ThemeProvider>;
