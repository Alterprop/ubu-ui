// @flow

import React from 'react';
import {
  StyledTitle,
  StyledTitleRow,
} from './styles';

import {
  Small,
} from '../Typography';

import {
  titlePropsDef,
} from './_util';

import {
  _omit,
} from '../_utils';

export type TitleProps = {
  pretitle?: string,
  subtitle?: string,
  title: ?string,
  order?: number,
  light?: boolean,
}

type Props = TitleProps;

export const Title = (props: Props) => (
    <StyledTitle {..._omit(props, titlePropsDef)}>
      { props.pretitle || props.subtitle
          ? <Small order={props.order}>
              { props.pretitle }
            </Small>
          : null
      }
      <StyledTitleRow {..._omit(props, titlePropsDef)}>{ props.title }</StyledTitleRow>
      { props.pretitle || props.subtitle
          ? <Small>{ props.subtitle }</Small>
          : null
      }
    </StyledTitle>
)

Title.defaultProps = {
  font: 1,
  weight: 'normal'
}
