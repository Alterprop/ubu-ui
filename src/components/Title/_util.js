export const titlePropsDef = [
  'column',
  'font',
  'title',
  'pretitle',
  'subtitle',
  'order',
  'light',
  'textCase',
  'weight',
];
