// @flow

import styled from 'styled-components';
import * as _style from '../_styles.utils';
import {H3} from '../Typography';

export const StyledTitle = styled(H3)`
    padding: ${_style.getPadding};
    margin: ${_style.getMargin};
    align-items: ${({ align }) => align};
    justify-content: ${({ justify }) => justify};
    font-family: ${_style.getFontFamily};
`;

export const StyledTitleRow = styled.span`
  font-weight: ${({ weight }) => weight};
  font-size: ${_style.getFontSize};
`;
