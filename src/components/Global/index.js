import { createGlobalStyle } from 'styled-components';
import * as _style from '../_styles.utils';

const Global = createGlobalStyle`
  * {
    box-sizing: border-box;
  }
  html,
  body,
  #root,
  #__next {
    margin: ${_style.getMargin};
    padding: ${_style.getPadding};
    height: ${_style.getHeight};
    width: ${_style.getWidth};
  }
  body {
    font-family: ${_style.getFontFamily};
    color: ${_style.getColor};
    overflow:hidden;
  }
  #root,
  #__next {
    display: ${_style.getDisplay};
  }
  #root,
  #__next {
    display: ${_style.getDisplay};
    flex-direction: ${_style.getDirection};
    overflow: ${({ oflow }) => oflow};
  }
`;

Global.defaultProps = {
  size: '100%',
  margin: 0,
  padding: 0,
  box: 'flex',
  column: true,
  oflow: 'auto',
  color: 'rgba(0, 0, 0, 0.76)',
  family: ['Ubuntu Condensed', 'Open Sans Condensed', 'sans-serif'],
}

export default Global;
