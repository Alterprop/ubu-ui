import React from 'react';
import {DatePicker} from './';
import {ThemeProvider} from 'styled-components';
import { ShapesProvider } from '../Icon/provider';
import Global from '../Global';
import {theme} from '../../theme';
import shapes from '../Icon/icons';

export default { title: 'DatePicker' };

export const rawCalendar = () => <ThemeProvider theme={theme}>
  <ShapesProvider shapes={shapes}>
    <Global />
    <DatePicker
      label="Fecha"
      hint="fecha de nacimiento"
      locale="es-UY"
      borderWidth={0}
      margin={1}
      raise={0}
      radius={0.3}
      color="rgba(0,0,0,0.76)"
      labelBackground="rgba(0,0,0,0.056)"
      inputBackground="yellow" />
  </ShapesProvider>
</ThemeProvider>;
