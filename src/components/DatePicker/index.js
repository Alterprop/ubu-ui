// @flow

import React, { useState, useEffect } from 'react';
import type { UbuiProps } from '../../types';
import { Input } from '../Input';
import { Dialog } from '../Dialog';
import { Calendar } from '../Calendar';

type DatePickerProps = {
  backgroundFilter: string,
  calendarStyles: any,
  displayOptions: any[],
  date: string,
  hint: string,
  initialDate: string,
  inputBackground: string,
  inputColor: string,
  label: string,
  labelBackground: string,
  labelColor: string,
  locale: string,
  name: string,
  onChange: Function,
  styles: any,
};

type Props = $Shape<DatePickerProps & UbuiProps>;

export const DatePicker = (props: Props) => {
  const [visible, setVisible] = useState(false);
  const [value, setValue] = useState([]);
  const _fromInput = React.createRef();
  const _toInput = React.createRef();
  const _hiddenInput = React.createRef();
  const {
    name,
    locale,
    onChange,
  } = props;
  useEffect(() => {
    const [first, last] = value;

    if (first && _fromInput.current && _hiddenInput.current) {
      _fromInput.current.value = first.toLocaleString(locale, props.displayOptions);
      _hiddenInput.current.value = (new Date(first)).getTime();
    }
    if (last && _toInput.current) {
      _toInput.current.value = last.toLocaleString(locale, props.displayOptions);
    }

    if (typeof onChange === 'function' && value.length) {
      const [from, to] = value;
      const propName = Array.isArray(name) ? name[0] : name;
      const propToName = Array.isArray(name) && name.length === 2
        ? name[1]
        : `${name}To`;
      onChange(
        !to
          ? ({ name: propName, value: from.getTime() })
          : [
              {
                name: propName,
                value: from.getTime()
              },
              {
                name: propToName,
                value: to.getTime()
              }
            ]
      )
    }
  }, [value])

  const {
    label = 'date',
    background = 'white',
    deco,
    borderWidth=0.1
  } = props;
  return <>
    <Input input={{
        type: 'text',
        label: Array.isArray(label) ? label[0] : label,
        hint: props.hint
      }}
      deco={deco}
      borderWidth={borderWidth}
      icon={props.icon}
      iconColor={props.iconColor}
      labelColor={props.labelColor}
      labelBackground={props.labelBackground}
      color={props.inputColor}
      background={props.inputBackground}
      margin={props.margin}
      raise={props.raise}
      radius={props.radius}
      disabled
      onActionClick={() => setVisible(true)}
      ref={_fromInput} />

    <Input input={{
        type: 'hidden',
        name: Array.isArray(name) ? name[0] : name,
      }}
      ref={_hiddenInput} />

    { Array.isArray(name) && name.length === 2 && value[1] ? <Input input={{
          type: 'text',
          name: name[1],
          label: Array.isArray(label) && label.length === 2 ? label[1] : label,
        }}
        ref={_toInput}
        onClick={() => setVisible(true)}
        borderWidth={borderWidth}
        deco={deco}
      /> : null }

     <Dialog
        show={visible}
        background={background}
        size={[30,20]}
        contentPadding={[1,0]}
        raise={4}
        radius={1}
        onClose={() => setVisible(false)}
        modal>

        <Calendar
          locale={props.locale}
          date={props.initialDate}
          {...(props.calendarStyles || {})}
          value={value}
          onChange={(values) => setValue(values)}
        />

    </Dialog>
  </>
}

DatePicker.defaultProps = {
  displayOptions: {
    weekday: 'long',
    year: 'numeric',
    month: 'long',
    day: 'numeric'
  },
  icon: 'calendar',
  iconColor: 'primary',
  inputColor: 'inherit',
  calendarStyles: {
    numberSize: 3,
    radius: 3
  }
}
