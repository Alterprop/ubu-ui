// @flow
import type {
  UbuiProps,
} from '../types';

import type {
  Theme,
} from '../theme';

export const _withUnit = (value: number | string, {
  unit
}: Theme) => value && typeof value === 'number' ? value + unit : value;

export const responsiveValue = (
  prop: number[],
  theme: Theme,
  func: Function,
  index: number,
  propName: string): string => {

  if (!prop) return '';
  const bps = Object.keys(theme.breakpoints);
  if (index === 0 || !theme.breakpoints || !bps.length) {
    return `${func(prop, theme, propName)};\n`;
  } else {

    const [min, max] = theme.breakpoints[bps[index - 1]];
    if (min || max) {
      return `
        @media ${min ? `(min-width:${ min + theme.unit }) ` : ''}${min && max ? 'and ' : ''}${max ? `(max-width:${max + theme.unit})` : ''} {
          ${propName}: ${func(prop, theme, propName)};
        };\n
      `;
    }
    return '';
  }
}

const _getRelativeSize = (size, { theme, full }) => {
  if (full) {
    return '100%';
  } else if (typeof size === 'string' && size.includes('%')) {
    return size;
  } else if (!isNaN(theme.dimBase*size)) {
    return theme.dimBase*size + theme.unit;
  }
  return size;
};

export const getSize = (prop: string, index: number) =>
  (props: UbuiProps) => _getRelativeSize(
    Array.isArray(props[prop])
      ? props[prop][index]
      : props[prop],
    props
  );

export const getWidth = getSize('size', 0);
export const getHeight = getSize('size', 1);
export const getMinWidth = getSize('minSize', 0);
export const getMinHeight = getSize('minSize', 1);

export const getColor = ({
  color = 'inherit',
  textHue = 500,
  theme={}
}: UbuiProps) => color && theme.palette && theme.palette[color]
  ? theme.palette[color][textHue]
  : color;

export const getBackgroundColor = ({
  background,
  gradient = '45deg',
  gradientType = 'linear',
  hue = 500,
  theme={}
}: UbuiProps) => {
  if (Array.isArray(background)) {
    const stops = background.map((bg) => {

      return theme.palette && theme.palette[bg]
        ? theme.palette[bg][hue]
        : bg;

      });

    return `${gradientType}-gradient(${gradientType === 'linear'
      ? [gradient, ...stops].join()
      : stops.join()})`
  }
  return background && theme.palette && theme.palette[background]
    ? theme.palette[background][hue]
    : background;
}

export const getBorder = ({
  bordered,
  borderStyle = 'solid',
  borderColor = 'currentColor',
  borderWidth,
  theme,
}: UbuiProps) => bordered || borderWidth
  ? `${borderStyle} ${borderColor} ${borderWidth ? borderWidth + theme.unit : ''}`.trim()
  : null;

export const getElevation = ({
  theme,
  raise,
}: UbuiProps) => typeof raise === 'number' && theme
  ? theme.elevations[raise]
  : null;

export const getElevationHover = ({
  theme,
  raise,
}: UbuiProps) => raise
  ? theme.elevations[raise*2 < 5 ? raise*2 : 5]
  : null;

export const getUniqueSize = (prop: string) => (props: UbuiProps) => props[prop]
  ? props.theme.dimAurea*props[prop] + props.theme.unit
  : null;

export const getFontSize = getUniqueSize('font');
export const getFontFamily = ({ family }: UbuiProps) => Array.isArray(family)
  ? family.join()
  : family;


export const getPadValue = (prop: number | string | any[], theme: Theme, propName: string, responsive?: number | boolean) => {

  if (Array.isArray(prop)) {
    return prop.reduce((acc, value, index) => {
      if (responsive || Array.isArray(value)) {
        return `${acc} ${responsiveValue(
          value,
          theme,
          getPadValue,
          index,
          propName
        )}`;
      }

      return `${acc} ${
        typeof value === 'number' && !isNaN(value) ? theme.dimBase*value + theme.unit : value
      }`
      }, '')
  }

  return typeof prop === 'number' && !isNaN(prop) ? theme.dimBase*prop + theme.unit : prop;
}

export const getRadius = ({
    rounded,
    radius,
    theme,
  }: UbuiProps) => {
    if(rounded) {
      return '50%';
    }
    return radius && theme
      ? getPadValue(radius, theme, 'border-radius')
      : null;
  }

export const getPadding = ({
  padding,
  responsive,
  theme,
}: UbuiProps) => padding || padding === 0
    ? getPadValue(padding, theme, 'padding', responsive)
    : null;

export const getMargin = ({
  margin,
  theme,
}: UbuiProps) => margin || margin === 0
    ? getPadValue(margin, theme, 'margin')
    : null;

export const getPosition = ({
  position,
  xyz,
  theme,
}: UbuiProps) => {

  if (position === null || typeof position === 'undefined') {
    return position;
  } else if (typeof position === 'string') {
    return `position: ${position};`;
  }

  const axis = ['top', 'left', 'bottom', 'right'];

  let finalPosition = axis.reduce(
    (
      acc,
      value,
      index
    ) => {
      const finalValue =  _withUnit(Array.isArray(position)
        ? position[index]
        : position
      , theme);

      return finalValue || finalValue === 0
        ? `${acc} ${value}:${finalValue};\n`
        : acc
    }, 'position: absolute;\n'
  );

  if (xyz && Array.isArray(xyz)) {
    const [x=0,y=0,z=0] = xyz;
    finalPosition = finalPosition + `transform: translate3d(${x},${y},${z});\n`;
  }

  return finalPosition;
}

export const getDisplay = ({
  display,
  box,
  inline,
}: UbuiProps) => (display || box) && inline ? `inline-${display || box}` : display || box;

export const getDirection = ({
    box,
    column,
}: UbuiProps) => box === 'flex' && column ? 'column' : null;
