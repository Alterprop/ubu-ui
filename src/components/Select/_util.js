export const inputProps = [
  'deco',
  'radius',
  'margin',
  'padding',
  'borderWidth',
  'labelPadding',
];
