// @flow

import React, {useState, useEffect} from 'react';
import { useSpring } from 'react-spring';
import { Input } from '../Input';
import {
  Option,
  Dropdown,
  SelectWrapper,
} from './styles';
import { _pick } from '../_utils';
import { inputProps } from './_util';
import type { UbuiProps } from '../../types';

type SelectProps = {
  onItemClick: Function,
  onChange: Function,
  input: any,
}

type Props = $Shape<SelectProps & UbuiProps>;

export const Select = (props: Props) => {
  const {
    onChange,
    input={
      options: []
    },
    onItemClick,
  }: Props = props;

  const _input = React.createRef();
  const _hidden = React.createRef();

  const [visible, setVisibility] = useState(false);
  const [selected, setSelected] = useState({ label: '' });

  useEffect(() => {
    if (selected.value) {
      if (_input.current && _hidden.current) {
        _input.current.value = selected.label;
        _hidden.current.value = selected.value;
      }
      if (typeof onChange === 'function') {
        onChange(input.name
          ? { [input.name]: selected.value }
          : selected
        );
      }
    }
    return () => setVisibility(false);
  }, [selected]);

  const _styleProps = useSpring({
    opacity:  visible ? 1 : 0,
    transform: `translate3d(0,${visible ? '0' : '-40%'},0)`
  });

  const handleItemClick = (option) => {
    setSelected(option);
    if (typeof onItemClick === 'function') {
      onItemClick(option);
    }
  };

  return (
    <SelectWrapper>
      <Input input={{
          type: 'text',
          label: input.label,
          hint: input.hint,
          placeholder: input.placeholder,
          autoComplete: 'off',
        }}
        {..._pick(props, inputProps)}
        onFocus={() => setVisibility(true)}
        onLabelClick={(event) => {
          event.preventDefault();
          if (_input.current) {
            _input.current.focus();
          }
        }}
        icon={props.icon}
        ref={_input}
      />
      <Input
        input={{ type: 'hidden', name: input.name, }}
        ref={_hidden}
      />
      { visible ? <Dropdown
          background="white"
          raise={3}
          style={_styleProps}
        >
        { input.options.map((option) => <Option
            key={option.name}
            onClick={() => handleItemClick({
              ...option,
              optionName: option.name,
              name: input.name
            })}
            name={option.name}
            value={option.value}>
            {option.label}
           </Option>)
        }
      </Dropdown> : null }
    </SelectWrapper>
  )
}

Select.defaultProps = {
  icon: 'down'
}
