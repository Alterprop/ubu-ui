import React from 'react';
import { action } from '@storybook/addon-actions';
import { Select } from './';
import { ThemeProvider } from 'styled-components';
import { Form } from '../Form';
import { ShapesProvider } from '../Icon/provider';
import Global from '../Global';
import {theme} from '../../theme';
import shapes from '../Icon/icons';

export default { title: 'Select' };

export const single = () => <ThemeProvider theme={theme}>
  <ShapesProvider shapes={shapes}>
    <Global />
    <Select input={{
      name: 'select',
      label: 'Select',
      options: [{
      value: 'uno',
      label: 'Uno',
      name: 'uno',
      }, {
        value: 'dos',
        label: 'Dos',
        name: 'dos',
      }]
    }}
      padding={0} />
  </ShapesProvider>
</ThemeProvider>;

export const onForm = () => <ThemeProvider theme={theme}>
  <ShapesProvider shapes={shapes}>
    <Global />
    <Form onSubmit={action('form.click')}>
      <Select input={{
          name: 'select',
          label: 'select',
          options: [{
          value: 'uno',
          label: 'Uno',
          name: 'uno',
          }, {
            value: 'dos',
            label: 'Dos',
            name: 'dos',
          }]
        }}
        deco
        padding={[0, 1]} />
    </Form>
  </ShapesProvider>
</ThemeProvider>;
