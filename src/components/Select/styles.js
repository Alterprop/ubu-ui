import styled from 'styled-components';
import * as _style from '../_styles.utils';
import { animated } from 'react-spring'

export const Dropdown = styled(animated.ul)`
  position: absolute;
  top: 100%;
  left: 0;
  width: 100%;
  background: ${_style.getBackgroundColor};
  box-shadow: ${_style.getElevation};
  padding: ${({
    padding,
    theme,
  }) => _style.getPadding({ padding, theme }) || 0};
  margin: ${({
    margin,
    theme,
  }) => _style.getPadding({ margin, theme }) || 0};
  opacity: 0;
  z-index: 100;
`;

export const Option = styled.li`
  list-style: none;
  line-height: ${({ theme }) => theme.dimAurea*3 + theme.unit};
  padding: ${({ theme }) => '0 ' + (theme.dimAurea + theme.unit)};
  transform: translate3d(0,'-16px',0);
  &:hover {
    background: rgba(0, 0, 0, 0.056);
  }
`;

export const SelectWrapper = styled.div`
  position: relative;
`;
