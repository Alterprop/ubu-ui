// @flow

import React from 'react';
import type { UbuiProps } from '../../types';
import { Button } from '../Button';
import { Icon } from '../Icon';
import {
  iconButtonPropDef,
  nextIconPropDef,
} from './_util';
import {
  _omit,
  _pick,
  _getPropsFromDef,
} from '../_utils';

type IconButtonProps = {
  nextIcon?: string,
};

type Props = IconButtonProps & UbuiProps;

export const IconButton = (props: Props) => (
  <Button
    {..._omit(props, _getPropsFromDef({ ...iconButtonPropDef, ...nextIconPropDef }))}
    icon
  >
    <Icon {..._pick(props, iconButtonPropDef)} />
    { props.nextIcon ? <Icon {..._pick(props, nextIconPropDef)} /> : null}
  </Button>
)

IconButton.defaultProps = {
  padding: 0,
}
