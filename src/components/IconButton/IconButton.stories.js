import React from 'react';
import { action } from '@storybook/addon-actions';
import { IconButton } from './';
import { ShapesProvider } from '../Icon/provider';
import { ThemeProvider } from 'styled-components';
import { theme } from '../../theme';
import shapes from '../Icon/icons';

export default { title: 'IconButton' };

export const regularIcon = () => <ThemeProvider theme={theme}>
  <ShapesProvider shapes={shapes}>
    <IconButton
      color="blue"
      size={6}
      background="red"
      icon="facebook"
      onClick={action('click')}
    />
  </ShapesProvider>
</ThemeProvider>

export const nextIcon = () => <ThemeProvider theme={theme}>
  <ShapesProvider shapes={shapes}>
    <IconButton
      color="white"
      size={6}
      background="black"
      icon="facebook"
      iconSize={2}
      nextIcon="next"
      rounded
      raise={3}
      onClick={action('click')}
    />
  </ShapesProvider>
</ThemeProvider>
