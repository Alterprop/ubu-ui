export const iconButtonPropDef = {
  shape: 'icon',
  size: 'iconSize',
  color: 'color',
  textHue: 'textHue',
};

export const nextIconPropDef = {
  ...iconButtonPropDef,
  shape: 'nextIcon',
}
