import styled from 'styled-components';
import * as _style from '../_styles.utils';

export const StyledInput = styled.input`
  border: 0;
  outline: 0;
  padding: ${_style.getPadding};
  color: ${_style.getColor};
  flex: ${({ flex=1, type }) => !['checkbox', 'radio'].includes(type) ? flex : null };
  font-size: 1em;
  background: transparent;
  font-family: inherit;

  &:-webkit-autofill {
      background-color: rgba(0, 0, 0, 0.056)!important;
  }

  &:focus {
    transition: .3s;
    & ~ label {
      &:first-of-type {
        color: ${({ theme }) => _style.getColor({ color: 'primary', theme })};
        letter-spacing: 1.3px;
        transition: .3s;
      }
    }
  }
  &:focus ~ span:before,
  &:focus ~ span:after {
    width: 50%;
  }
  &[type="checkbox"],
  &[type="radio"] {
    display: none;
    & ~ label {
      &:first-of-type {
        padding-left: ${({ theme }) => theme.dimBase + theme.unit};
      }
      &:nth-of-type(2) {
        width: ${({ theme }) => theme.dimAurea + theme.unit};
      }
    }
  }

  &:checked {
    & ~ label {
      &:nth-of-type(2) {
        border-color: ${({
          theme,
          background = 'primary'
        }) => _style.getBackgroundColor({ theme, background })};
        &::before {
          opacity: 1;
          transition: .3s;
          background: ${({
            theme,
            background = 'primary'
          }) => _style.getBackgroundColor({ theme, background })};
          border-radius: ${({ type }) => type === 'radio' ? '50%' : null };
        }
      }
    }
  }

  &:disabled {
    border-bottom-style: dotted;
    background: transparent;
  }

  ${({deco}) => deco ? `
    border-bottom: 2px solid rgba(0, 0, 0, 0.056);
    &:focus {
      border-color: transparent;
    }
  ` : null};
`;

export const StyledInputWrapper = styled.span`
  display: ${({
    type,
    inline,
    hidden,
  }) => {
    if (hidden) {
      return 'none';
    }
    return ['radio'].includes(type) || inline ? 'inline-flex': 'flex';
  }};
  flex-direction: ${({
    type,
    deco,
    direction = 'column',
  }) => !['checkbox', 'radio'].includes(type) && deco ? direction : null };
  position: ${({ deco }) => !deco ? 'relative' : null};
  background: ${_style.getBackgroundColor};
  color: ${_style.getColor};
  padding: ${_style.getPadding};
  margin: ${_style.getMargin};
  box-shadow: ${_style.getElevation};
  border-radius: ${_style.getRadius};
  border: ${(props) => !['checkbox', 'radio'].includes(props.type) && !props.deco
    ? _style.getBorder(props)
    : null
  };
  ${({ type }) => ['checkbox', 'radio'].includes(type) ? `align-items: center;` : null}
  ${(props) => !props.deco && ['checkbox', 'radio'].includes(props.type)
      ? `padding:0;`
      : null
  };
  @media (max-width: 480px) {
    flex-direction: ${({ deco }) => !deco ? 'column' : null};
  }
`;

export const StyledLabel = styled.label`
  display: flex;
  flex-direction: ${({ direction }) => direction};
  justify-content: ${({ justify = 'flex-end' }) => justify};
  order: ${({ type }) => ['checkbox', 'radio'].includes(type) ? 2 : null};
  background: ${_style.getBackgroundColor};
  padding: ${_style.getPadding};
  border: ${_style.getBorder};
  color: ${_style.getColor};
  font-size: small;
  text-transform: uppercase;
  text-align: right;
  transition: .3s;
  button {
    padding: 0 0.5rem 0 1rem;
  }
`;

export const StyledLabelContent = styled.span`
  display: flex;
  flex-direction: column;
  justify-content: ${({ justify = 'center' }) => justify};
`;

export const CheckboxLabel = styled.label`;
  width: ${({ theme }) => theme.dimAurea + theme.unit};
  height: ${({ theme }) => theme.dimAurea + theme.unit};
  border: 1px solid currentColor;
  position: relative;
  border-radius: ${({
    type
  }) => type === 'radio' ? '50%' : null};

  &::before {
    content: '';
    position: absolute;
    display: inline-block;
    opacity: 0;
  }
  &::before {
    width: ${({ theme }) => theme.dimBase + theme.unit};
    height: ${({ theme }) => theme.dimBase + theme.unit};
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  }
`;

export const Hint = styled.small`
  text-transform: uppercase;
  color: ${_style.getColor};
  ${_style.getPosition};
  max-width: ${({ deco, theme }) => !deco ? theme.dimAurea*6 + theme.unit : null};
  @media (max-width: 480px) {
    max-width: 100%;
  }
`;

export const StyledDeco = styled.span`
  position: relative;
  display: inline-block;
  width: 100%;
  &:before,
  &:after {
    content: '';
    height: 2px;
    width: 0;
    bottom: 0;
    position: absolute;
    background: ${_style.getColor};
    transition: 0.2s ease all;
  }
  &:before {
    left: 50%;
  }
  &:after {
    right: 50%;
  }
`;
