import React from 'react';
import { action } from '@storybook/addon-actions';
import { Input } from './';
import {ThemeProvider} from 'styled-components';
import { ShapesProvider } from '../Icon/provider';
import Global from '../Global';
import {theme} from '../../theme';
import shapes from '../Icon/icons';

export default { title: 'Input' };

const input = {
  type: 'text',
  label: 'input',
  name: 'input',
  hint: 'input hint',
  placeholder: 'input',
};

const nohint = {
  type: 'text',
  label: 'nolabel',
  name: 'input-nolabel',
  placeholder: 'input',
};

const checkbox = {
  type: 'checkbox',
  label: 'checkbox',
  name: 'checkbox',
  placeholder: 'input'
};

const radio = {
    type: 'radio',
    label: 'radio',
    id: 'radio',
    name: 'radio',
    value: 'radio',
}

const antiradio = {
    type: 'radio',
    label: 'antiradio',
    id: 'antiradio',
    name: 'radio',
    value: 'antiradio',
}

export const simpleInput = () => <ThemeProvider theme={theme}>
  <ShapesProvider shapes={shapes}>
    <Global padding={2}/>
    <Input
      input={input}
      onChange={action('text.onChange')}
      deco />
    <Input
      input={{ ...input, name: 'input2' }}
      onChange={action('text.onChange')}
      icon="twitter"
      iconColor="primary" />
    <Input
      input={{ ...nohint }}
      onChange={action('nohint.onChange')}
      icon="search"
    />
    <Input
      input={checkbox}
      onChange={action('checkbox.onChange')}
      deco />
  </ShapesProvider>
</ThemeProvider>;

export const simpleCheckbox = () => <ThemeProvider theme={theme}>
  <ShapesProvider shapes={shapes}>
  <Global padding={2}/>
    <Input
      input={checkbox}
      deco />
    <Input
      input={radio}
      inline />
    <Input
      input={antiradio}
      inline />
  </ShapesProvider>
</ThemeProvider>;
