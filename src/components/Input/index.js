// @flow

import React from 'react';
import type { UbuiProps } from '../../types';
import Ink from 'react-ink';
import { IconButton } from '../IconButton';
import {
  Hint,
  CheckboxLabel,
  StyledDeco,
  StyledLabel,
  StyledLabelContent,
  StyledInputWrapper,
  StyledInput,
} from './styles';
import {
  buttonPropsDef,
  wrapperPropDef,
  labelPropDef,
  inputPropDef,
} from './_util';
import {
  _pick,
  _omit,
} from '../_utils';


type InputProps = {
  deco: boolean,
  hintColor: string,
  iconColor: string,
  input: any,
  inputColor: string,
  labelColor: string,
  labelHue: number,
  labelPadding: number | number[],
  onActionClick: Function,
  onBlur: Function,
  onChange: Function,
  onClick: Function,
  onFocus: Function,
  onLabelClick: Function,
  padding: number | number[],
  target: any,
};

type Props = InputProps & UbuiProps;

// eslint-disable-next-line react/display-name
export const Input: any = React.forwardRef((props: Props, ref) => {
  const {
    input = {},
    onActionClick,
  }: Props = props;
  const getValue = ({
    target,
    name,
    type
  }) => {
    const propByType = {
      checkbox: 'checked',
    }
    if (typeof props.onChange === 'function') {
      props.onChange({ [name]: target[propByType[type] || 'value'] })
    }
  };

  const _onActionClick = (event) => {
    event.preventDefault();
    if (typeof onActionClick === 'function') {
      event.stopPropagation();
      onActionClick();
    }
  }

  const renderInput = () => (
    <StyledInput
      autoComplete="off"
      {..._omit(input, ['label'])}
      {..._pick(props, inputPropDef)}
      ref={ref}
      id={input.id || input.name}
      padding={props.padding || (props.deco ? [1,0.5] : [1,1,1,2])}
      deco={props.deco}
      onClick={props.onClick}
      onFocus={props.onFocus}
      onBlur={props.onBlur}
      onChange={({ target }) => getValue({ target, ...input })}
    />
  )
  return (
    <StyledInputWrapper
        {..._pick(props, wrapperPropDef)}
        hidden={input.type === 'hidden'}
        type={input.type}
      >

      { renderInput() }

      { props.deco && !['checkbox', 'radio'].includes(input.type)
          ? <StyledDeco
              color={props.decoColor || 'primary' }
            />
          : null }

      { input.label
          ? <StyledLabel
              {..._pick(props, labelPropDef)}
              type={input.type}
              onClick={props.onLabelClick}
              htmlFor={input.id || input.name}
              >
              <StyledLabelContent>
                {`${input.label} ${input.required ? '*' : ''}`.trim()}
                {
                  input.hint
                    ? <Hint
                        deco={props.deco}
                        color={props.hintColor}
                      >{input.hint}</Hint>
                    : null
                }
              </StyledLabelContent>
              { props.icon
                  ?  <IconButton
                        {..._pick(props, buttonPropsDef)}
                        color={props.iconColor}
                        padding={[0, 2]}
                        onClick={_onActionClick}
                      />
                  : null }

            </StyledLabel>
          : null
      }

      { ['checkbox', 'radio'].includes(input.type)
          ? <CheckboxLabel
              htmlFor={input.id || input.name}
              type={input.type}>
              <Ink />
            </CheckboxLabel>
          : null }
    </StyledInputWrapper>
  )
})

Input.defaultProps = {
  borderColor: 'rgba(0, 0, 0, 0.16)',
  borderWidth: 0.05,
  margin: [1,0],
  iconSize: 2,
  inputPadding: [0.5, 1],
  labelPadding: [0.5, 1],
}
