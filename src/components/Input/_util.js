export const wrapperPropDef = [
  'background',
  'hue',
  'color',
  'textHue',
  'bordered',
  'borderWidth',
  'borderColor',
  'inline',
  'margin',
  'deco',
  'raise',
  'radius',
];

export const labelPropDef = {
  color: 'labelColor',
  textHue: 'labelTextHue',
  padding: 'labelPadding',
  fontSize: 'labelFontSize',
  background: 'labelBackground',
};

export const inputPropDef = {
  color: 'inputColor',
  textHue: 'inputTextHue',
  margin: 'inputMargin',
  disabled: 'disabled',
  required: 'required',
}

export const buttonPropsDef = [
  'icon',
  'iconSize',
  'nextIcon'
]
