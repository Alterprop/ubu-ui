import styled from 'styled-components';
import * as _style from '../_styles.utils';


export const LineNavItem = styled.li`
  text-orientation: upright;
  writing-mode: tb-rl;
  writing-mode: vertical-rl;
  letter-spacing: 0px;
  list-style: none;
  cursor: pointer;
  color: ${props => props.active ? 'white' : 'rgba(255,255,255,0.36)'};
  padding: ${_style.getPadding};
  font-weight: ${({ weight }) => weight};
  transition: 0.3s;
`;

export const LineNavList = styled.ul`
  margin: ${_style.getMargin};
  padding: ${_style.getPadding};
  ${_style.getPosition};
`;

LineNavList.defaultProps = {
  position: [0,null,null,1]
}

LineNavList.defaultProps = {
  margin: 0,
  padding: 0,
}

export const LineNavLine = styled.span`
  display: inline-block;
  height: ${_style.getHeight};
  width: ${_style.getWidth};
  background: ${props => props.active ? 'white' : 'rgba(255,255,255,0.36)'};
  border-radius: 3px;
  transition: 0.4s;
`;

LineNavLine.defaultProps = {
  size: [0.3,11]
}
