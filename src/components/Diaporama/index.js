// @flow
import React, { Fragment } from 'react';
import { Background } from '../Background';
import type { UbuiProps } from '../../types';
import { _pick } from '../_utils';
import { backgroundPropDef } from './_util';
import { ViewPager } from '../ViewPager';

type Props = {
  ...UbuiProps,
  backgroundIndex?: number,
  index: number,
  items: any[],
  onChange: () => number,
}

export const Diaporama = (props: Props) => {
  const { items = [] } = props;
  return <ViewPager
    index={props.index}
    onChange={props.onChange}>
      {props.children}
      { items.map((item, index) =>
          <Fragment key={`${item.name}-${index}`}>
            {item.content}
            <Background
              {..._pick(item, backgroundPropDef)}
              index={props.backgroundIndex}
            />
          </Fragment>
      )}
    </ViewPager>
}

Diaporama.defaultProps = {
  index: 0,
  box: 'flex',
  backgroundIndex: -1,
}
