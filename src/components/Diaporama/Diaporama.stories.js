import React, { useState } from 'react';
import { action } from '@storybook/addon-actions';
import {Diaporama} from './';
import {Button} from '../Button';
import {ThemeProvider} from 'styled-components';
import {theme} from '../../theme';

export default { title: 'Diaporama' };

const diapos = [
  {
    imageUrl: 'https://images.pexels.com/photos/296878/pexels-photo-296878.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260' ,
    backgroundFilter: 'grayscale(0.9)',
    position: 2,
    styles: {
      color: 'red'
    },
    content: <h3>ITEM</h3>
  },
  { imageUrl: 'https://images.pexels.com/photos/1509428/pexels-photo-1509428.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260' },
]

export const simpleFigure = () => {
  const [current, setCurrent] = useState(0);
  return <ThemeProvider theme={theme}>
    <Diaporama
      items={diapos}
      index={current}
      onChange={(index) => {
        setCurrent(index.current);
        action('change')
      }}
    >
    </Diaporama>
    <div><Button onClick={() => {
      setCurrent(current === 0 ? 1 : 0);
      action(current);
    }}>Change</Button></div>
    <div>{current}</div>
  </ThemeProvider>

}
