import styled from 'styled-components';
import * as _style from '../_styles.utils';

export const StyledForm = styled.form`
  background: ${_style.getBackgroundColor};
  color: ${_style.getColor}
  border: ${_style.getBorder};
  padding: ${_style.getPadding};
  margin: ${_style.getMargin};
  width: ${_style.getWidth};
`;

export const StyledFormFooter = styled.footer`
  text-align: right;
`
