// @flow

import React from 'react';
import { FormFooter } from './FormFooter';
import { Container } from '../Layout';
import {
  _pick,
  unflatten,
} from '../_utils';
import {
  formPropDef,
  formFooterPropDef,
} from './_util';

type FormProps = {
  onCancel?: Function,
  onSubmit: Function,
  children: any,
  schema?: boolean,
  passEvent?: boolean,
}

type Props = FormProps;

export const Form = (props: Props) => {
  const {
    schema,
  }: Props = props;

  const _form = React.createRef();

  const _reset = () => {
    if (_form.current && typeof _form.current.reset === 'function') {
      _form.current.reset();
    }
  }

  const _onSubmit = (event) => {
    event.preventDefault();
    if (props.passEvent) {
      if (typeof props.onSubmit === 'function') {
        props.onSubmit(event)
      }
      return;
    }

    const { elements } = event.target;
    const values = [].slice.call(elements)
      .filter((element) => element.name)
      .reduce((acc, element) => {
        if (!element.value || (['radio'].includes(element.type) && !element.checked)) {
          return acc;
        }

      const value = ({
        ...acc,
        [element.name]: ['checkbox'].includes(element.type)
          ? element.checked
          : element.value
      });

      return schema ? unflatten(value) : value;
    }, {});

    if (typeof props.onSubmit === 'function') {
      props.onSubmit(values, event)
    }

    _reset();
  }

  const _onCancel = (event) => {
    event.preventDefault();
    if (typeof props.onCancel === 'function') {
      props.onCancel(event);
    }
    _reset();
  }

  return (
      <Container
        as="form"
        {..._pick(props, formPropDef)}
        onSubmit={_onSubmit}
        ref={_form}>

        {props.children}

        <FormFooter
          {..._pick(props, formFooterPropDef)}
          onCancel={props.onCancel ? _onCancel : null}
        />

    </Container>
  )
}
