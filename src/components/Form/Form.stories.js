import React from 'react';
import { action } from '@storybook/addon-actions';
import {Form} from './';
import {Input} from '../Input';
import {Select} from '../Select';
import {DatePicker} from '../DatePicker';
import {ThemeProvider} from 'styled-components';
import {ShapesProvider} from '../Icon/provider';
import Global from '../Global';
import {theme} from '../../theme';
import shapes from '../Icon/icons';

export default { title: 'Form' };

const input = {
  type: 'text',
  label: 'input',
  name: 'input',
  hint: 'input hint',
  placeholder: 'input',
};

const nohint = {
  type: 'text',
  label: 'nolabel',
  name: 'input-nolabel',
  placeholder: 'input',
};

const checkbox = {
  type: 'checkbox',
  label: 'checkbox',
  name: 'checkbox',
  placeholder: 'input'
};

export const simpleForm = () => <ThemeProvider theme={theme}>
  <ShapesProvider shapes={shapes}>
    <Global padding={2}/>
    <Form
        onSubmit={action('values')}
        onCancel={action('canceled')}
        padding={[1,2]}
    >
      <Input
        input={{
          ...input,
          hint: 'A valid facebook account'
        }}
        deco />
        <DatePicker
          label="Fecha"
          hint="Fecha de nacimiento"
          date="2019-12-03"
          name="datetime"/>
      <Input
        input={{ ...input, name: 'input2', required: true }}
        icon="twitter"
        iconColor="white"
        borderWidth={0}
        labelColor="rgba(0,0,0,0.76)"
        hintColor="rgba(0,0,0,0.56)"
        background="yellow"
        padding={[2,1]} />
        <Input
          input={{
            ...input,
            name: 'input3',
            label: 'Twitter',
            hint: 'A valid twitter account'
          }}
          icon="twitter"
          iconColor="rgba(0,0,0,0.56)"
          borderWidth={0}
          background="gray" />
      <Input
        input={{ ...nohint }}
        icon="search"
        labelColor="rgba(0,0,0,0.56)"
        iconColor="rgba(0,0,0,0.56)"
        labelBackground="#f2f2f2"
      />
      <Select input={{
        name: 'select',
        label: 'País',
        hint: 'Elige una opción',
        options: [{
          value: 'uno',
          label: 'Uno',
          name: 'uno',
        }, {
          value: 'dos',
          label: 'Dos',
          name: 'dos',
        }]
        }}
        />

        <Input
          input={checkbox}
          deco />
    </Form>
  </ShapesProvider>
</ThemeProvider>;

export const loginForm = () => <ThemeProvider theme={theme}>
  <ShapesProvider shapes={shapes}>
    <Global padding={2}/>
    <Form
        submitText="Vamos"
        cancelText="Vamos atrás"
        cancelColor="red"
        submitBackground="yellow"
        submitColor="rgba(0,0,0,0.56)"
        onSubmit={action('values')}
        onCancel={action('canceled')}
        padding={[1,2]}
    >
      <Input
        input={{
          type: 'text',
          name: 'username',
          label: 'Username',
          required: true
        }} />
      <Input
        input={{
          type: 'password',
          name: 'password',
          label: 'password',
          required: true,
        }} />
    </Form>
  </ShapesProvider>
</ThemeProvider>;

export const yellowForm = () => <ThemeProvider theme={theme}>
  <ShapesProvider shapes={shapes}>
    <Global padding={2}/>
    <Form
        submitText="Vamos. ¡Ya!"
        submitBackground="rgba(255,255,255,0.36)"
        submitColor="#ff3600"
        color="white"
        onSubmit={action('values')}
        padding={[3,2]}
        background={["#ff3600", "yellow"]}
        gradient="45deg"
        gradientType="linear"
        buttonBorder={0}
        buttonPadding={2.3}
        full
    >
      <Input
        input={{
          type: 'text',
          name: 'username',
          label: 'Username',
          required: true
        }}
        icon="email"
        background="rgba(0,0,0,0.056)"
        borderWidth={0}
        padding={[2,1]}/>
      <Input
        input={{
          type: 'password',
          name: 'password',
          label: 'password',
          required: true,
        }}
        icon="password"
        background="rgba(0,0,0,0.056)"
        borderWidth={0}
        padding={[2,1]}
      />
    </Form>
  </ShapesProvider>
</ThemeProvider>;
