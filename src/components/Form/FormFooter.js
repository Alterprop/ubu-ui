// @flow

import React from 'react';
import { Container } from '../Layout';
import { Button } from '../Button';
import type { UbuiProps } from '../../types';

type FormProps = {
  cancelBackground: string | string[],
  cancelColor: string,
  cancelText: string,
  cancelTextHue?: number,
  cancelHue?: number,
  buttonBorder: number,
  buttonPadding: number | number[],
  footerPadding: number | number[],
  onCancel: ?Function,
  onSubmit: Function,
  submitText: string,
  submitBackground: string,
  submitColor: string,
  submitHue?: number,
  submitTextHue: number,
}

type Props = $Shape<FormProps & UbuiProps>;

export const FormFooter = (props: Props) => (
  <Container
    as="footer"
    padding={props.footerPadding}
    >

    { typeof props.onCancel === 'function'
        ? <Button
            borderWidth={props.buttonBorder}
            background={props.cancelBackground}
            hue={props.cancelHue}
            color={props.cancelColor || 'white'}
            textHue={props.cancelTextHue}
            padding={props.buttonPadding}
            radius={props.radius}
            full={props.full}
            ink={props.ink}
            onClick={props.onCancel}>
            {props.cancelText || 'Cancel' }
          </Button>
        : null
    }

    <Button
      background={props.submitBackground}
      hue={props.submitHue}
      color={props.submitColor}
      textHue={props.submitTextHue}
      padding={props.buttonPadding}
      radius={props.radius}
      full={props.full}
      ink={props.ink}>
      {props.submitText}
    </Button>

  </Container>
)

FormFooter.defaultProps = {
  submitBackground: 'primary',
  submitColor: 'white',
  submitText: 'Accept',
  cancelColor: 'primary',
  cancelText: 'Cancel',
  buttonPadding: [1,2],
  buttonBorder: 0.05,
  footerPadding: [0.5, 0],
  full: false,
  radius: 0,
  ink: true,
}
