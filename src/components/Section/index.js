// @flow
import React from 'react';
import type { UbuiProps } from '../../types';
import { Background } from '../Background';
import { Title } from '../Title';
import { FlexBox, Flex } from '../Layout';
import {
  _pick,
} from '../_utils';
import {
  sectionPropsDef,
} from './_util';

// TODO - urgent props definition

type PropsDef = {
  ...UbuiProps,
  aside: any,
  row: boolean | number,
  notitle: boolean,
  title: any,
  overlay: UbuiProps,
  content: UbuiProps,
  backgroundPosition?: number | string | any[],
  backgroundFilter?: string,
  backgroundAttachment?: boolean,
  backgroundIndex: number,
};

type Props = PropsDef;

export const Section = (props: Props) => {
  return (
    <FlexBox {..._pick(props, sectionPropsDef)}>
      { props.title && !props.notitle ? <Title {...props.title}>{props.icon}</Title> : null }
      <Flex {...props.content}>{ props.children }</Flex>
      { props.aside }
      { props.image || props.background ? <Background
          background={props.background}
          imageUrl={props.image}
          fixed={props.backgroundAttachment}
          position={props.backgroundPosition}
          backgroundFilter={props.backgroundFilter}
          index={props.backgroundIndex}
        /> : null }
      { props.overlay ? <Background {...props.overlay} /> : null }
    </FlexBox>
  )
}

Section.defaultProps = {
  size: ['100%', null],
  minSize: [null, '100%'],
  position: 'relative',
}
