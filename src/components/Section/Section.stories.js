import React from 'react';
import { Section } from './';
import {ThemeProvider} from 'styled-components';
import {theme} from '../../theme';
import Global from '../Global';

export default { title: 'Section' };

const title = {
  title: 'Section',
  pretitle: 'una sección',
  subtitle: 'simple',
  color: 'white'
};

const content = {
  box: 'flex',
  flex: '1',
  align: 'center',
  justify: 'center',
  color: 'white',
  background: 'rgba(34,65,102,0.26)',
  raise: 3,
  radius: 3
};

const overlay = {
  background: 'white',
  filter: 'blur(3px)',
  position: 16,
  transparency: 0.3,
}

export const section = () => <ThemeProvider theme={theme}>
  <Global />
  <Section
    background="red"
    title={title}
    content={content}
    overlay={overlay}
    box="flex"
    column
    oflow="hidden"
    padding={[[1,2], [3,4], [6,8]]}
    margin={[[1,2], [2,3], [3,4]]}
    image="https://upload.wikimedia.org/wikipedia/commons/9/96/Jacques-Louis_David_004.jpg"
    backgroundFilter="grayscale(0.5)"
  >
    El contenido
  </Section>
</ThemeProvider>
