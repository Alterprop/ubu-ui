import styled from 'styled-components';
import * as _style from '../_styles.utils';

export const StyledCardContent = styled.div`
  margin: 0;
  padding: ${_style.getPadding};
`;

export const StyledCardFooter = styled.footer`
  margin: 0;
  padding: ${_style.getPadding};
  text-align: right;
`;

export const StyledCard = styled.div`
  display: ${_style.getDisplay};
  flex-direction: ${_style.getDirection};
  align-items: ${({ align }) => align };
  justify-content: ${({ justify }) => justify };
  margin: ${_style.getMargin};
  padding: ${_style.getPadding};
  position: ${({ backgroundImage }) => backgroundImage ? 'relative' : null };
  border: ${_style.getBorder};
  border-radius: ${_style.getRadius};
  overflow: hidden;
  width: ${_style.getWidth};
  min-height: ${({ theme }) => theme.dimAurea*15 + theme.unit };
  box-shadow: ${_style.getElevation};
  color: ${_style.getColor};
  background: ${_style.getBackgroundColor};
  ${
    ({ theme, margin }) => `
      @media (max-width: ${theme.dimBase*theme.breakpoints.mobile + theme.unit} ) {
        width: 100%;
        margin: ${(Array.isArray(margin) ? margin[1] : margin) + theme.unit + ' 0'};
      }
    `
  };
`;

StyledCard.defaultProps = {
  box: 'flex',
  column: true,
}
