export const cardPropsDef = [
  'align',
  'background',
  'backgroundImage',
  'bordered',
  'borderWidth',
  'borderStyle',
  'borderColor',
  'color',
  'hue',
  'justify',
  'margin',
  'padding',
  'raise',
  'size',
  'textHue',
];

export const cardHeaderPropsDef = [
  'avatar',
  'avatarRadius',
  'avatarSize',
  'headerPadding',
  'title',
  'subtitle',
  'pretitle',
]
