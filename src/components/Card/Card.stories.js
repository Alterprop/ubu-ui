import React from 'react';
import {Card} from './';
import {Button} from '../Button';
import {ThemeProvider} from 'styled-components';
import Global from '../Global';
import {theme} from '../../theme';

export default { title: 'Card' };

const card = {
  content: {
    title: 'Léonidas aux Thermopyles',
    subtitle: 'Jacques-Louis David',
    image: 'https://upload.wikimedia.org/wikipedia/commons/9/96/Jacques-Louis_David_004.jpg',
    avatar: 'https://upload.wikimedia.org/wikipedia/commons/c/c6/David_Self_Portrait.jpg',
    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquid ex ea commodi consequat.',
  },
  theme: {
    "avatarRadius": [3],
    "margin": [0,2],
    "rounded": true,
    "ink": true,
    "raise": 1,
    "size": 42
  }
}
export const regularCard = () => <ThemeProvider theme={theme}>
  <Global />
  <Card {...{
    name: 'uno',
    ...card.theme,
    ...card.content
  }} />
</ThemeProvider>

export const headerCard = () => <ThemeProvider theme={theme}>
  <Global />
  <Card {...{
    name: 'uno',
    ...card.theme,
    ...card.content,
    contentPadding: 2,
    topHeader: true,
    footerPadding: 1,
    footer: <Button
      ink
      background="gray"
      padding={[1.5,3]}
      onClick={() => console.log('hola')}>
      <strong>HOLA</strong>
    </Button>
  }} />
</ThemeProvider>
