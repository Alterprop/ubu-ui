// @flow

import React from 'react';
import { Avatar } from '../Avatar';
import { Background } from '../Background';
import { Figure } from '../Figure';
import { Title } from '../Title';
import {
  StyledCardContent,
  StyledCardFooter,
  StyledCard,
} from './styles';

import {
  Flex,
  FlexBox,
} from '../Layout';

import {
  Paragraph,
} from '../Typography';

import {
  cardPropsDef,
  cardHeaderPropsDef,
} from './_util';
import { _pick } from '../_utils';
import type { UbuiProps } from '../../types';

type CardProps = {
  avatar: string,
  avatarRadius: number | number[],
  avatarSize: number | number[],
  backgroundFilter: string,
  contentPadding: number | number[],
  footer: any,
  footerPadding: number | number[],
  headerPadding: number | number[],
  items: any,
  pretitle?: string,
  subtitle?: string,
  text?: string,
  title: ?string,
  topHeader: boolean | number,
};

type Props = CardProps & UbuiProps;

export const CardHeader = (props: Props) => (
  <FlexBox
    as="header"
    padding={props.headerPadding}>
    <Flex as="span" align="center">
      { props.avatar ? <Avatar
          imageUrl={props.avatar}
          radius={props.avatarRadius}
          size={props.avatarSize}
        /> : null }
      <Title
        title={props.title}
        subtitle={props.subtitle}
        pretitle={props.pretitle}
        padding={1}
        light
        flex={1} />
    </Flex>
  </FlexBox>
)

export const Card = (props: Props) => (
  <StyledCard
    {..._pick(props, cardPropsDef)}>
    { props.topHeader && props.title
        ? <CardHeader
            {..._pick(props, cardHeaderPropsDef)}
            padding={props.headerPadding} />
        : null
    }
    { props.image
      ?  <Figure
          size={['100%', 'auto']}
          minSize={20}
          flex={1}
          imageUrl={props.image} />
      : null
    }
    { props.backgroundImage
      ?  <Background imageUrl={props.backgroundImage} filter={props.backgroundFilter} />
      : null
    }
    { !props.topHeader && props.title
        ? <CardHeader
          {..._pick(props, cardHeaderPropsDef)} />
        : null
    }

    <StyledCardContent
      padding={props.contentPadding}>
      {props.text ? <Paragraph>{props.text}</Paragraph> : null}
      {props.children}
    </StyledCardContent>

    { props.footer
        ? <StyledCardFooter
            padding={props.footerPadding}>
            {props.footer}
          </StyledCardFooter>
        : null
    }
  </StyledCard>
)

Card.defaultProps = {
  contentPadding: [0, 2, 2],
  footerPadding: [2,1,1],
  headerPadding: 1,
}
