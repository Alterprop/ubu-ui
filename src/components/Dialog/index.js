// @flow
import React, { createRef } from 'react';
import { useTransition } from 'react-spring'
import { Portal } from '../Portal';
import { Title } from '../Title';
import { Button } from '../Button';
import type {
  UbuiProps,
  SectionProps,
} from '../../types';
import animations from '../transitions.json';
import {
  Backdrop,
  StyledDialog,
  DialogHeader,
  DialogFooter,
  DialogContent,
} from './styles';

type DialogProps = {
  actions: any[],
  animation: string,
  children: any,
  closeFunc: Function,
  closeText: string,
  closeColor: string,
  modal: boolean,
  onClose: Function,
  pretitle: string,
  title: string,
  subtitle: string,
  show: boolean,
}

type Props = $Shape<DialogProps & UbuiProps & SectionProps>;

export const Dialog = (props: Props) => {
  const {
    children,
    onClose,
    show,
    actions= [],
  }: Props = props;
  const _dialog = createRef();
  const transitions = useTransition(
    show,
    null,
    props.position ? animations[props.animation] : animations.dialog[props.animation]
  );
  return transitions.map(({ item, key, props: _styleProps }) => item ? <Portal key={key}>
        <Backdrop
          position={props.backdropPosition}
          background={props.backdropBackground}
          style={_styleProps}
          onClick={onClose} />

        <StyledDialog
          background={props.background}
          padding={props.padding}
          size={props.size}
          raise={props.raise}
          radius={props.radius}
          position={props.position}
          style={_styleProps}
          ref={ _dialog }>
          { props.title ? <DialogHeader
              padding={props.headerPadding}
              margin={props.headerMargin}>
             <Title
                title={props.title}
                subtitle={props.subtitle}
                pretitle={props.pretitle}
              />
          </DialogHeader>
            : null
          }
          <DialogContent
            padding={props.contentPadding}
            margin={props.contentMargin}>
            {children}
          </DialogContent>

          { onClose || actions.length ? <DialogFooter
            padding={props.footerPadding}
            margin={props.footerMargin}>

            {actions.map(
              (action) => <Button
                key={action.name}
                color='primary'
                ink
                onClick={() => {
                  action.onClick();
                  if (typeof onClose === 'function') {
                    onClose();
                  }
                }}>
                {action.text}
              </Button>)
            }
            { onClose ? <Button
                onClick={props.onClose}
                color={props.closeColor}
                ink>
                {props.closeText || 'ok'}
              </Button>
              : null
            }
          </DialogFooter> : null }
        </StyledDialog>
      </Portal> : null)
}

Dialog.defaultProps = {
  background: 'white',
  contentPadding: 2,
  footerPadding: 2,
  headerPadding: 2,
  raise: 1,
  radius: 0.5,
  size: [30, 20],
  actions: [],
  animation: 'bottom',
}
