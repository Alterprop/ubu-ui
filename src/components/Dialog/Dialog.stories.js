import React from 'react';
import { action } from '@storybook/addon-actions';
import {Dialog} from './';
import {ThemeProvider} from 'styled-components';
import Global from '../Global';
import {theme} from '../../theme';

export default { title: 'Dialog' };

export const simpleFigure = () => <ThemeProvider theme={theme}>
  <Global />
  <Dialog
    show={true}
    title="Dialog"
    subtitle="A simple dialog"
    onClose={action('click')} >
    Este es un diálogo simple.
  </Dialog>
</ThemeProvider>;
