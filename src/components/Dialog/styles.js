// @flow
import styled from 'styled-components';
import * as _style from '../_styles.utils';
import { animated } from 'react-spring';

export const Backdrop = styled.div`
  ${_style.getPosition};
  background: ${_style.getBackgroundColor};
`;

Backdrop.defaultProps = {
  background: 'rgba(0,0,0,0.056)',
  position: 0,
};

export const StyledDialog = styled(animated.div)`
  position: absolute;
  display: flex;
  flex-direction: column;
  justify-content: ${({ justify = 'space-between' }) => justify};
  background: ${_style.getBackgroundColor};
  color: ${_style.getColor};
  top: 50%;
  left: 50%;
  transform: translate3d(-50%, -50%, 0);
  box-shadow: ${_style.getElevation};
  padding: ${_style.getPadding};
  min-width: ${_style.getWidth};
  min-height: ${_style.getHeight};
  border-radius: ${_style.getRadius};
  ${_style.getPosition};
`;

StyledDialog.defaultProps = {
  position: ['50%','50%']
}

export const DialogHeader = styled.header`
  padding: ${_style.getPadding};
`;

export const DialogFooter = styled.footer`
  display: flex;
  justify-content: flex-end;
  align-items: center;
  padding: ${_style.getPadding};
`;

export const DialogContent = styled.span`
  display: ${_style.getDisplay};
  flex-direction: ${({ direction }) => direction};
  justify-content: ${({ justify }) => justify};
  flex: ${({ flex }) => flex};
  padding: ${_style.getPadding};
`;

DialogContent.defaultProps = {
  display: 'flex',
  direction: 'column',
  justify: 'center',
}
