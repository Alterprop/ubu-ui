// @flow
import type { Theme } from './theme';
export type Color = string;

export type Hue = {
  "100"?: string,
  "200"?: string,
  "300"?: string,
  "400"?: string,
  "500": string,
  "600"?: string,
  "700"?: string,
  "800"?: string,
  "900"?: string,
}

export type Palette = { [Color]: Hue }

export type Size = {
  small: number | string,
  normal: number | string,
  big: number | string,
}

export type BreakPoints = {
  mobile: number,
  landscape: number,
  tablet: number,
  desktop: number,
  wide: number,
}

export type UbuiPropsType = {
  align: string,
  as: string,
  background: Color | Array<Color>,
  backgroundAttachment: string,
  backgroundImage: string,
  bordered: boolean | number,
  borderWidth: number | string,
  borderStyle: 'solid' | 'dashed' | 'dotted',
  borderColor: Color,
  box: string,
  children: any,
  color: Color,
  column: boolean,
  count: number,
  deco: boolean | number,
  decoColor: string,
  display: string,
  backgroundFilter: string,
  flex: number | string,
  fontSize: number | string,
  weight: number,
  family: string | string[],
  flowx: string | boolean,
  flowy: string | boolean,
  full: boolean | number,
  gradient: string,
  gradientType: 'linear' | 'radius',
  hue: number,
  icon: string | any,
  iconColor: string,
  iconHue: number,
  iconSize: number | string | any[],
  image: string,
  ink: boolean | number,
  inline: boolean,
  justify: string,
  light: boolean,
  margin: ?number | ?string | ?any[],
  nextIcon: string | any,
  oflow: ?string,
  onClick: Function,
  padding: number | string | any[],
  position: number | string | any[],
  radius: number | number[],
  raise: number,
  responsive: boolean | number,
  rounded: boolean,
  size: number | string | any[],
  spacing: string,
  textCase: 'uppercase' | 'lowercase',
  textHue: number,
  theme: Theme,
  xyz: any[],
};

export type SectionProps = {
  backdropPosition: number | number[],
  backdropBackground: string,
  contentPadding: number | number[],
  contentMargin: number | number[],
  footerPadding: number | number[],
  footerMargin: number | number[],
  headerPadding: number | number[],
  headerMargin: number | number[],
  itemPadding: number | number[],
  itemMargin: number | number[],
}

export type UbuiProps = $Shape<UbuiPropsType>;
